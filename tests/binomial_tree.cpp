// -*- C++ -*-

#define CXX_UNIT_TEST

#include <driver/test.h>
#include <driver/memory.h>
#include <driver/array.h>

#include <cxx/heap/impl/binomial_tree.h>

#include <math.h>

#include <iostream>
#include <iterator>
#include <algorithm>
#include <functional>
#include <vector>

struct Test : ::cxx::test::instance
{
    explicit Test (const char* s) : ::cxx::test::instance (s) { }
};

/**************************************************************************/

struct user_def
{ 
    user_def () : m_value () { }

    user_def (int val) : m_value (val) { }

    user_def (user_def const& ref) : m_value (ref.m_value) { }

    int value () const { return m_value; }

    user_def& operator++ () {
        ++m_value;
        return *this;
    }

    user_def operator++ (int) {
        user_def tmp (*this);
        return ++*this, tmp;
    }

    int operator+ (int arg) const {
        return m_value + arg;
    }

    int operator- (int arg) const {
        return m_value - arg;
    }

    int operator/ (int arg) {
        return m_value/arg;
    }

protected:

    int m_value;
};

inline bool 
operator== (user_def const& lhs, user_def const& rhs)
{
    return lhs.value () == rhs.value ();
}

inline bool 
operator!= (user_def const& lhs, user_def const& rhs)
{
    return !(lhs == rhs);
}

inline bool 
operator< (user_def const& lhs, user_def const& rhs)
{
    return lhs.value () < rhs.value ();
}

/**********************************************************************/

template< typename Tree > 
void check_empty (Test& test, Tree const& t, bool expected)
{
    CXX_TEST (test, t.empty () == expected);
}

template< typename Tree > 
void check_size (Test& test, Tree const& t, size_t expected)
{
    CXX_TEST (test, t.size () == expected);
}

template< typename Tree >
void check_property (Test& test, Tree& t)
{
    typedef Tree tree_type;

    typedef typename tree_type::iterator       iterator;
    typedef typename tree_type::node_pointer   node_pointer;

    for (iterator iter = t.begin (); iter != t.end (); ++iter) {
        node_pointer p = iter.m_node;

        CXX_TEST (test, p->child [0] == 0 || 
                  !t.key_comp ()(p->child [0]->p->key (), p->key ()));

        CXX_TEST (test, p->child [0] == 0 || p->degree > p->child [0]->degree);
        CXX_TEST (test, p->child [1] == 0 || p->degree < p->child [1]->degree);
    }
}

template< typename Tree, typename Array >
void check_traversal (Test& test, Tree& t, Array& a)
{
    typedef typename Array::keys_type::iterator array_iterator;

    typedef typename Tree::node_pointer   node_pointer;
    typedef typename Tree::key_reference key_reference;
    typedef typename Tree::iterator tree_iterator;

    typedef std::map< typename Tree::key_type, int > counters;
    typedef typename counters::iterator counters_iterator;

    counters c;
    for (array_iterator iter = a.keys ().begin (); 
         iter != a.keys ().end (); ++iter) {
        ++c [*iter];
    }

    size_t i = 0;

    for (tree_iterator iter = t.begin (); iter != t.end (); ++iter, ++i) 
        --c [key_reference ()(*iter)];

    CXX_TEST(test, i == t.size ());

    for (counters_iterator iter = c.begin (); iter != c.end (); ++iter) {
        CXX_TEST(test, iter->second == 0);
    }
}

template< typename Tree, typename Array >
void check_reverse_traversal (Test& test, Tree& t, Array& a)
{
    typedef typename Array::keys_type::iterator array_iterator;

    typedef typename Tree::key_reference key_reference;
    typedef typename Tree::reverse_iterator tree_reverse_iterator;

    typedef std::map< typename Tree::key_type, int > counters;
    typedef typename counters::iterator counters_iterator;

    counters c;
    for (array_iterator iter = a.keys ().begin (); 
         iter != a.keys ().end (); ++iter) {
        ++c [*iter];
    }

    size_t i = 0;

    for (tree_reverse_iterator iter = t.rbegin (); iter != t.rend (); ++iter, ++i)
        --c [key_reference ()(*iter)];

    CXX_TEST(test, i == t.size ());

    for (counters_iterator iter = c.begin (); iter != c.end (); ++iter) {
        CXX_TEST(test, iter->second == 0);
    }
}

/**************************************************************************/

template< typename T, typename U, typename Off, typename C = std::less< T > > 
struct TypeHelper
{
    typedef T key_type;
    typedef U value_type;

    typedef Off offset_type;
    typedef C key_compare;

    typedef cxx::impl::key_ref< U, T > key_reference;
    typedef cxx::impl::binomial_tree< 
        key_type, value_type, offset_type, key_reference, key_compare > 
    tree_type;
};

template< typename T, typename U, typename Off, typename C >
struct TypeHelper< T, std::pair< T, U >, Off, C >
{
    typedef T key_type;
    typedef std::pair< T, U > value_type;

    typedef Off offset_type;
    typedef C key_compare;

    typedef cxx::impl::key_ref< value_type, key_type > key_reference;

    typedef cxx::impl::binomial_tree< 
        key_type, value_type, offset_type, key_reference, key_compare > 
    tree_type;
};

/**************************************************************************/

template< typename Tree >
void test_default_ctor (Test& test)
{
    CXX_STEP (("Testing default ctor."));

    Tree t;

    CXX_TEST(test, t.size () == 0);

    check_empty (test, t, true);
    check_size (test, t, 0);

    typename Tree::iterator beg = t.begin ();
    typename Tree::iterator end = t.end ();

    typename Tree::reverse_iterator rbeg = t.rbegin ();
    typename Tree::reverse_iterator rend = t.rend ();

    CXX_TEST (test, beg  == end);
    CXX_TEST (test, rbeg == rend);

    check_property (test, t);
}

template< typename Tree >
void test_range_ctor (Test& test, size_t size)
{
    CXX_STEP (("Testing range ctor, size %zu.", size));

    typedef typename Tree::key_type       key_type;
    typedef typename Tree::value_type     value_type;

    typedef typename Tree::key_reference  key_reference;
    typedef typename Tree::key_compare    key_compare;
    
    typedef typename Tree::pointer        pointer;

    typedef
        cxx::test::sequence_generator< key_type, value_type > 
        generator_type;

    typedef 
        cxx::test::array< key_type, value_type, key_reference, generator_type >
        array_type;

    array_type a (size);
    std::random_shuffle (a.begin (), a.end ());

    Tree t (a.begin (), a.end ());

    check_empty (test, t, false);
    check_size (test, t, a.keys_count ());
    check_property (test, t);

    check_traversal (test, t, a);
    check_reverse_traversal (test, t, a);
}

template< typename Tree >
void test_copy_ctor (Test& test, size_t size)
{
    CXX_STEP (("Testing copy ctor, size %zu.", size));

    typedef typename Tree::key_type       key_type;
    typedef typename Tree::value_type     value_type;

    typedef typename Tree::key_reference  key_reference;
    typedef typename Tree::key_compare    key_compare;
    
    typedef typename Tree::pointer        pointer;

    typedef
        cxx::test::sequence_generator< key_type, value_type > 
        generator_type;

    typedef 
        cxx::test::array< key_type, value_type, key_reference, generator_type >
        array_type;

    array_type a (size);
    std::random_shuffle (a.begin (), a.end ());

    Tree tmp (a.begin (), a.end ());
    Tree t (tmp);

    check_empty (test, t, false);
    check_size (test, t, a.keys_count ());
    check_property (test, t);

    check_traversal (test, t, a);
    check_reverse_traversal (test, t, a);
}

template< typename Tree >
void test_change_key (Test& test, size_t size)
{
    CXX_STEP (("Testing change-key, size %zu.", size));

    typedef typename Tree::key_type       key_type;
    typedef typename Tree::value_type     value_type;

    typedef typename Tree::key_reference  key_reference;
    typedef typename Tree::key_compare    key_compare;
    
    typedef typename Tree::pointer        pointer;

    typedef
        cxx::test::sequence_generator< key_type, value_type > 
        generator_type;

    typedef 
        cxx::test::array< key_type, value_type, key_reference, generator_type >
        array_type;

    array_type a (size);
    Tree t (a.begin (), a.end ());

    while (!t.empty ()) {
        typename Tree::iterator iter = t.begin ();

        size_t n = size_t (rand () % t.size ());
        for (size_t i = 0; i < n && iter != t.end (); ++iter, ++i) ;

        t.change_key (iter, -int (size));
        CXX_TEST (test, iter == t.top ());

        t.pop ();
        break;
    }
}

/**********************************************************************/

template< typename K, typename T, typename Off, typename C >
void test (Test& t)
{
    typedef TypeHelper< K, T, Off, C > type_helper;
    typedef typename type_helper::tree_type tree_type;

    test_default_ctor< tree_type > (t);

    for (size_t size = 4; size <= 32 * 1024; size *= 2) {
        test_range_ctor< tree_type > (t, size);
        test_copy_ctor < tree_type > (t, size);
        test_change_key< tree_type > (t, size);
    }
}

int main ()
{
    ::srand (time (0));

    Test t ("Binomial tree test.");

    test< int, int, int, std::less< int > > (t);
    test< int, std::pair< int, int >, int, std::less< int > > (t);
    test< int, std::pair< int, user_def >, int, std::less< int > > (t);
    test< user_def, user_def, int, std::less< user_def > > (t);

    return 0;
}
