// -*- mode: c++ -*-

#define CC_UNIT_TEST 1

#include <detail/test.hpp>
#include <detail/basic_array.hpp>
#include <detail/predicated_sort.hpp>

#include <cc/detail/splay_tree.hpp>

#include <cmath>

#include <iostream>
#include <algorithm>

/**********************************************************************/

struct Test : unit_test_t
{
    explicit Test (const char* s) : unit_test_t (s) { }
};

/**********************************************************************/

struct user_def 
{ 
    user_def () : m_value () { }

    user_def (int val) : m_value (val) { }

    user_def (user_def const& ref) : m_value (ref.m_value) { }

    int value () const { return m_value; }

    user_def& operator++ () {
        ++m_value;
        return *this;
    }

    user_def operator++ (int) {
        user_def tmp (*this);
        return ++*this, tmp;
    }

    int operator/ (int arg) {
        return m_value/arg;
    }

protected:

    int m_value;
};

inline bool 
operator== (user_def const& lhs, user_def const& rhs)
{
    return lhs.value () == rhs.value ();
}

inline bool 
operator!= (user_def const& lhs, user_def const& rhs)
{
    return !(lhs == rhs);
}

inline bool 
operator< (user_def const& lhs, user_def const& rhs)
{
    return lhs.value () < rhs.value ();
}

/**********************************************************************/

template< typename Tree > 
void check_empty (Test& test, Tree const& t, bool expected)
{
    CC_TEST (test, t.empty () == expected);
}

template< typename Tree > 
void check_size (Test& test, Tree const& t, size_t expected)
{
    CC_TEST (test, t.size () == expected);
}

template< typename Tree >
void check_property (Test& test, Tree& t)
{
    typedef Tree tree_type;

    typedef typename tree_type::key_compare    key_compare;

    typedef typename tree_type::iterator       iterator;
    typedef typename tree_type::const_iterator const_iterator;

    typedef typename tree_type::node_type      node_type;
    typedef typename tree_type::node_pointer   node_pointer;

    node_pointer header = t.tree_header ();
    CC_TEST(test, header->p->p == header);

    for (iterator iter = t.begin (); iter != t.end (); ++iter) {
        node_pointer node   = iter.m_node;
        node_pointer left   = node->child [0];
        node_pointer right  = node->child [1];

        CC_TEST(test, left  == 0 || left->p  == node);
        CC_TEST(test, right == 0 || right->p == node);

        int mask = (left ? 2 : 0) | (right ? 1 : 0);

        switch (mask) {
        case 1:
            CC_TEST(test, !t.key_comp ()(right->key (), node->key ()));
            break;
        case 2:
            CC_TEST(test, !t.key_comp ()(node->key (), left->key ()));
            break;
        case 3:
            CC_TEST(test, !t.key_comp ()(node->key (), left->key ()));
            CC_TEST(test, !t.key_comp ()(right->key (), node->key ()));
            break;
        default:
            break;
        }
    }
}

template< typename Tree, typename Array >
void check_traversal (Test& test, Tree& t, Array& a, int period)
{
    typedef typename Tree::key_reference key_reference;

    bool dup = a.size () == t.size ();

    size_t i, count;
    i = count = 0;

    typename Tree::iterator iter = t.begin ();
    for (; iter != t.end (); ++iter, i += dup ? 1 : period, ++count) {
        CC_TEST(test, key_reference ()(*iter) == key_reference ()(a [i]));
    }

    CC_TEST(test, count == t.size ());
}

template< typename Tree, typename Array >
void check_reverse_traversal (Test& test, Tree& t, Array& a, int period)
{
    typedef typename Tree::key_reference key_reference;

    size_t i, count;

    i = a.size () - 1;
    count = 0;

    bool dup = a.size () == t.size ();

    typename Tree::reverse_iterator riter = t.rbegin ();
    for (; riter != t.rend (); ++riter, i -= dup ? 1 : period, ++count) {
        CC_TEST(test, key_reference ()(*riter) == key_reference ()(a [i]));
    }

    CC_TEST(test, count == t.size ());
}

/**************************************************************************/

template< typename T, typename U, typename C > 
struct TypeHelper
{
    typedef T key_type;
    typedef U value_type;

    typedef C key_compare;

    typedef cc::detail::key_ref< U, T > key_reference;
    typedef cc::detail::splay_tree< T, U, key_reference, key_compare > tree_type;
};

template< typename T, typename U, typename C >
struct TypeHelper< T, std::pair< T, U >, C >
{
    typedef T key_type;
    typedef std::pair< T, U > value_type;

    typedef C key_compare;

    typedef cc::detail::key_ref< value_type, key_type > key_reference;

    typedef cc::detail::splay_tree<
        key_type, value_type, key_reference, key_compare > tree_type;
};

/**************************************************************************/

template< typename Tree >
void test_default_ctor (Test& test)
{
    CC_STEP (("Testing default ctor."));

    Tree t;

    CC_TEST(test, t.size () == 0);

    check_empty (test, t, true);
    check_size (test, t, 0);

    typename Tree::iterator beg = t.begin ();
    typename Tree::iterator end = t.end ();

    typename Tree::reverse_iterator rbeg = t.rbegin ();
    typename Tree::reverse_iterator rend = t.rend ();

    CC_TEST (test, beg  == end);
    CC_TEST (test, rbeg == rend);

    check_property (test, t);
}

template< typename Tree >
void test_range_ctor (Test& test, size_t size, bool dup)
{
    CC_STEP (("Testing range ctor, size %zu, %s duplication ... ",
               size, (dup ? "with" : "without")));

    typedef typename Tree::key_type       key_type;
    typedef typename Tree::value_type     value_type;

    typedef typename Tree::key_reference     key_reference;
    typedef typename Tree::key_compare    key_compare;
    
    typedef typename Tree::pointer        pointer;

    typedef
        cc::test::alternate_generator< key_type, value_type >
        generator_type;

    typedef 
        cc::test::array< key_type, value_type, key_reference, generator_type >
        array_type;

    for (int factor = 1; factor < 10; ++factor) {

        array_type a (size, generator_type (factor));
        std::random_shuffle (a.begin (), a.end ());

        Tree t (a.begin (), a.end (), dup);

        cc::test::sort< value_type, key_compare, key_reference > ()(
            a.begin (), a.end ());

        check_empty (test, t, false);
        check_size (test, t, dup ? a.keys_count () : a.uniq_keys_count ());
        check_property (test, t);

        check_traversal (test, t, a, factor);
        check_reverse_traversal (test, t, a, factor);
    }
}

template< typename Tree >
void test_copy_ctor (Test& test, size_t size, bool dup)
{
    CC_STEP (("Testing copy ctor, size %zu, %s duplication ... ",
               size, (dup ? "with" : "without")));

    typedef typename Tree::key_type       key_type;
    typedef typename Tree::value_type     value_type;

    typedef typename Tree::key_reference     key_reference;
    typedef typename Tree::key_compare    key_compare;
    
    typedef typename Tree::pointer        pointer;

    typedef
        cc::test::alternate_generator< key_type, value_type >
        generator_type;

    typedef 
        cc::test::array< key_type, value_type, key_reference, generator_type >
        array_type;

    for (int factor = 1; factor < 10; ++factor) {
        array_type a (size, generator_type (factor));
        std::random_shuffle (a.begin (), a.end ());

        Tree tmp (a.begin (), a.end (), true);
        Tree t (tmp, dup);

        cc::test::sort< value_type, key_compare, key_reference > ()(
            a.begin (), a.end ());

        check_empty (test, t, false);
        check_size (test, t, dup ? a.keys_count () : a.uniq_keys_count ());
        check_property (test, t);

        check_traversal (test, t, a, factor);
        check_reverse_traversal (test, t, a, factor);
    }
}

template< typename Tree >
void test_insert (Test& test, size_t size)
{
    CC_STEP (("Insertion into a splay tree (%zu elements).", size));

    typedef Tree tree_type;
            
    typedef typename Tree::key_type       key_type;
    typedef typename Tree::value_type     value_type;

    typedef typename Tree::key_reference     key_reference;
    typedef typename Tree::key_compare    key_compare;
    
    typedef typename Tree::pointer        pointer;

    typedef typename tree_type::iterator iterator;

    typedef 
        cc::test::alternate_generator< key_type, value_type >
        generator_type;

    typedef 
        cc::test::array< key_type, value_type, key_reference, generator_type >
        array_type;

    typedef typename array_type::keys_type keys_type;
    typedef typename array_type::uniq_keys_type uniq_keys_type;

    typedef typename keys_type::iterator keys_iterator;
    typedef typename uniq_keys_type::iterator uniq_keys_iterator;

    array_type a (size, generator_type (2));
    std::random_shuffle (a.begin (), a.end ());

    Tree t;

    cc::test::sort< value_type, key_compare, key_reference > ()(
        a.begin (), a.end ());

    check_empty (test, t, true);
    check_size (test, t, 0);
    check_property (test, t);
        
    iterator iter = t.begin ();

    keys_iterator keys_iter = a.keys ().begin ();
    keys_iterator uniq_keys_iter = a.uniq_keys ().begin ();

    size_t i = 0;
    for (; i < a.size () && keys_iter != a.keys ().end (); ++keys_iter, ++i) {

        bool dup = false;
        size_t prev = t.size ();

        iterator result = t.find (*keys_iter);
        if (result != t.end ()) {

            t.insert (a [i], false);

            check_empty (test, t, prev == 0);
            check_size (test, t, prev);
            check_property (test, t);

            dup = true;
        }

        t.insert (a [i], dup);

        check_empty (test, t, prev + 1 == 0);
        check_size (test, t, prev + 1);
        check_property (test, t);

        result = t.find (*keys_iter);
        CC_TEST(test, result != t.end ());
    }
}

template< typename Tree >
void test_erase (Test& test, size_t size)
{
    CC_STEP (("Erasure from a splay tree (size %zu).", size));

    typedef typename Tree::key_type       key_type;
    typedef typename Tree::value_type     value_type;

    typedef typename Tree::key_reference     key_reference;
    typedef typename Tree::key_compare    key_compare;
    
    typedef typename Tree::pointer        pointer;
    typedef typename Tree::iterator       iterator;

    typedef 
        cc::test::alternate_generator< key_type, value_type >
        generator_type;

    typedef 
        cc::test::array< key_type, value_type, key_reference, generator_type >
        array_type;

    typedef typename array_type::keys_type keys_type;
    typedef typename array_type::uniq_keys_type uniq_keys_type;

    typedef typename keys_type::iterator keys_iterator;
    typedef typename uniq_keys_type::iterator uniq_keys_iterator;

    const int factor = 2;

    array_type a (size, generator_type (factor));
    std::random_shuffle (a.begin (), a.end ());

    Tree t (a.begin (), a.end (), true);

    cc::test::sort< value_type, key_compare, key_reference > ()(
        a.begin (), a.end ());

    check_empty (test, t, false);
    check_size (test, t, a.keys_count ());
    check_property (test, t);
        
    keys_iterator keys_iter = a.keys ().begin ();
    keys_iterator uniq_keys_iter = a.uniq_keys ().begin ();

    for (; uniq_keys_iter != a.uniq_keys ().end (); ++uniq_keys_iter) {
        size_t prev = t.size ();

        size_t erased = t.erase (*uniq_keys_iter);
        CC_TEST (test, erased == std::min (prev, size_t (factor)));

        erased = std::min (prev, size_t (factor));

        iterator result = t.find (*uniq_keys_iter);
        CC_TEST(test, result == t.end ());

        check_empty (test, t, prev - erased == 0);
        check_size  (test, t, prev - erased);

        check_property (test, t);
    }

    check_empty (test, t, true);
    check_size (test, t, 0);
    check_property (test, t);
}

template< typename Tree >
void test_find (Test& test, size_t size)
{
    CC_STEP (("Testing find, size %zu.", size));

    typedef typename Tree::key_type       key_type;
    typedef typename Tree::value_type     value_type;
    
    typedef typename Tree::key_reference     key_reference;
    typedef typename Tree::key_compare    key_compare;
    
    typedef typename Tree::pointer        pointer;
    typedef typename Tree::iterator       iterator;
    
    typedef typename Tree::difference_type difference_type;

    typedef 
        cc::test::sequence_generator< key_type, value_type >
        generator_type;
    
    typedef 
        cc::test::array< key_type, value_type, key_reference, generator_type >
        array_type;
    
    array_type a (size);
    std::random_shuffle (a.begin (), a.end ());

    Tree t; 

    cc::test::sort< value_type, key_compare, key_reference > ()(
        a.begin (), a.end ());
    
    for (size_t count = 1; count < 3; ++count) {
        t.insert (a.begin (), a.end (), true);

        for (size_t i = 0; i < a.size (); ++i) {
            key_type k = key_reference ()(a [i]);
        
            iterator iter = t.find (k);
            CC_TEST (test, iter != t.end ());
        
            std::pair< iterator, iterator > interval = t.equal_range (k);

            difference_type diff = std::distance (interval.first, interval.second);
            CC_TEST (test, diff == difference_type (count));
        }
    }
}

template< typename Tree, typename Iterator >
bool 
test_exceptions (Test& test, Iterator first, Iterator last, int counter)
{
    using namespace ::cc::test;

    {
        Tree t;

        try {
            for (Iterator iter = first; iter != last; ++iter) {
                t.insert (*iter, true);
                check_property (test, t);
            }
        }
        catch (...) {
            check_property (test, t);
            return false;
        }
    }

    return true;
}

template< typename Tree >
void
test_exceptions (Test& test, size_t size)
{
    CC_STEP (("Testing exception safety, size %zu.", size));

    typedef typename Tree::key_type       key_type;
    typedef typename Tree::value_type     value_type;

    typedef typename Tree::key_reference     key_reference;
    typedef typename Tree::key_compare    key_compare;
    
    typedef typename Tree::pointer        pointer;

    typedef 
        cc::test::alternate_generator< key_type, value_type >
        generator_type;

    typedef 
        cc::test::array< key_type, value_type, key_reference, generator_type >
        array_type;

    array_type a (size, generator_type (1));

    for (int counter = 2; 
         !test_exceptions< Tree > (test, a.begin (), a.end (), counter);
         ++counter) ;
}

/**********************************************************************/

template< typename K, typename T, typename C >
void test (Test& t, bool dup)
{
    typedef TypeHelper< K, T, C > type_helper;
    typedef typename type_helper::tree_type tree_type;

    test_default_ctor< tree_type > (t);

    for (size_t size = 1; size <= 1024; size *= 2) {
        test_range_ctor< tree_type > (t, size, dup);
        test_copy_ctor < tree_type > (t, size, dup);
        test_insert    < tree_type > (t, size);
        test_erase     < tree_type > (t, size);
        test_find      < tree_type > (t, size);
    }

    test_exceptions< tree_type > (t, 100);
}

template< typename K, typename T, typename C >
void test (Test& t)
{
    test< K, T, C > (t, true);
    test< K, T, C > (t, false);
}

int main ()
{
    Test t ("Splay tree test.");

    test< int, int, std::less< int > > (t);
    test< int, std::pair< int, int >, std::less< int > > (t);
    test< int, std::pair< int, user_def >, std::less< int > > (t);
    test< user_def, user_def, std::less< user_def > > (t);

    return 0;
}
