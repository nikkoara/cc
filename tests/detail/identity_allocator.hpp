// -*- mode: c++ -*-

#ifndef CC_TESTS_DETAIL_COUNTED_ALLOCATOR_HPP
#define CC_TESTS_DETAIL_COUNTED_ALLOCATOR_HPP

namespace cc {
namespace test {

template< typename T >
struct identity_allocator : public T
{
    typedef T base_allocator;

    typedef typename base_allocator::size_type size_type;
    typedef typename base_allocator::difference_type difference_type;

    typedef typename base_allocator::pointer pointer;
    typedef typename base_allocator::const_pointer const_pointer;

    typedef typename base_allocator::reference reference;
    typedef typename base_allocator::const_reference const_reference;

    typedef typename base_allocator::value_type value_type;

    identity_allocator () : instance (instance_counter++) { }

    identity_allocator (identity_allocator const& other)
        : base_allocator (other), instance (other.instance) { }

    identity_allocator& operator= (identity_allocator const& other) {
        instance = other.instance;
    }

    template< typename U >
    friend bool 
    operator== (identity_allocator< U > const&, identity_allocator< U > const&);

    template< typename U >
    friend bool
    operator!= (identity_allocator< U > const&, identity_allocator< U > const&);

    template< typename U > 
    struct rebind { 
        typedef identity_allocator< std::allocator< U > > other;
    };

private:
    int instance;
    static int instance_counter;
};

template< typename T >
/* static */ int identity_allocator< T >::instance_counter /* = 0 */;

template< typename T >
inline bool
operator== (identity_allocator< T > const& lhs, identity_allocator< T > const& rhs)
{
    return lhs.instance == rhs.instance;
}

template< typename T >
inline bool
operator!= (identity_allocator< T > const& lhs, identity_allocator< T > const& rhs)
{
    return !(lhs == rhs);
}

}}

#endif // CC_TESTS_DETAIL_IDENTITY_ALLOCATOR_HPP
