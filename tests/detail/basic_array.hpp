// -*- C++ -*-

#ifndef CC_TESTS_DETAIL_BASIC_ARRAY_HPP
#define CC_TESTS_DETAIL_BASIC_ARRAY_HPP

#include <assert.h>
#include <stdlib.h>
#include <time.h>

#include <set>

namespace cc {
namespace test {

template< typename T, typename U = T >
struct default_generator
{
    typedef T key_type;
    typedef U value_type;

    value_type operator() () const {
        return value_type ();
    }
};

template< typename T, typename U >
struct default_generator< T, std::pair< T, U > >
{
    typedef T key_type;
    typedef U mapped_type;

    typedef std::pair< key_type, mapped_type > value_type;

    value_type operator() () const {
        return value_type (key_type (), mapped_type ());
    }
};

template< typename T, typename U = T >
struct ident_generator
{
    typedef T key_type;
    typedef U value_type;

    explicit
    ident_generator (key_type const& k = key_type ()) : m_default (k) { }

    value_type operator() () const {
        return value_type (m_default);
    }

private:

    key_type m_default;
};

template< typename T, typename U >
struct ident_generator< T, std::pair< T, U > >
{
    typedef T key_type;
    typedef U mapped_type;

    typedef std::pair< key_type, mapped_type > value_type;

    explicit
    ident_generator (key_type const& k = key_type ()) : m_default (k) { }

    value_type operator() () const {
        return value_type (key_type (m_default), mapped_type ());
    }

private:

    key_type m_default;
};

template< typename T, typename U = T >
struct random_generator
{
    typedef T key_type;
    typedef U value_type;

    explicit
    random_generator (bool do_seed = false) {
        if (do_seed) 
            ::srand (unsigned (::time (0)));
    }

    value_type operator() () const {
        return value_type (::rand ());
    }
};

template< typename T, typename U >
struct random_generator< T, std::pair< T, U > >
{
    typedef T key_type;
    typedef U mapped_type;

    typedef std::pair< key_type, mapped_type > value_type;

    explicit
    random_generator (bool do_seed = false) {
        if (do_seed) srand (unsigned (::time (0)));
    }

    value_type operator() () const {
        return value_type (rand (), mapped_type ());
    }
};

template< typename T, typename U = T >
struct sequence_generator
{
    typedef T key_type;
    typedef U value_type;

    explicit
    sequence_generator (key_type val = key_type ()) : m_internal (val) { }

    value_type operator() () const {
        return value_type (m_internal++);
    }

private:
    mutable key_type m_internal;
};

template< typename T, typename U >
struct sequence_generator< T, std::pair< T, U > >
{
    typedef T key_type;
    typedef U mapped_type;

    typedef std::pair< key_type, mapped_type > value_type;

    explicit
    sequence_generator (key_type val = key_type ()) : m_internal (val) { }

    value_type operator() () const {
        return value_type (m_internal++, mapped_type ());
    }

private:
    mutable key_type m_internal;
};

template< typename T, typename U = T >
struct alternate_generator
{
    typedef T key_type;
    typedef U value_type;

    explicit
    alternate_generator (int factor, key_type val = key_type ())
        : m_factor (factor), m_internal (val) { 
    }

    value_type operator() () const {
        return value_type (m_internal++/m_factor);
    }

private:
    int m_factor;
    mutable key_type m_internal;
};

template< typename T, typename U >
struct alternate_generator< T, std::pair< T, U > >
{
    typedef T key_type;
    typedef U mapped_type;

    typedef std::pair< key_type, mapped_type > value_type;

    explicit
    alternate_generator (int factor, key_type val = key_type ())
        : m_factor (factor), m_internal (val) { 
    }

    value_type operator() () const {
        return value_type (m_internal++/m_factor, mapped_type ());
    }

private:
    int m_factor;
    mutable key_type m_internal;
};

/**********************************************************************/

template< typename K, typename T, typename KeyOf, 
          typename G = random_generator< T >, 
          typename C = std::less< K >,
          typename A = std::allocator< T > >
struct array
{
    typedef K key_type;
    typedef T value_type;

    typedef C key_compare;

    typedef G generator_type;
    typedef A allocator_type;

    typedef typename allocator_type::pointer          pointer;
    typedef typename allocator_type::const_pointer    const_pointer;

    typedef typename allocator_type::reference        reference;
    typedef typename allocator_type::const_reference  const_reference;

    typedef typename allocator_type::size_type         size_type;
    typedef typename allocator_type::difference_type   difference_type;

    typedef std::set< key_type, key_compare > uniq_keys_type;
    typedef std::multiset< key_type, key_compare > keys_type;

    typedef pointer iterator;
    typedef const_pointer const_iterator;

public:

    explicit
    array (size_t, 
           generator_type const& = generator_type (),
           key_compare    const& = key_compare (),
           allocator_type const& = allocator_type ());

    ~array ();

    pointer get () { return m_array; }

    size_type size () const { return m_size; }

    reference operator[] (size_type n) {
        return m_array [n];
    }

    const_reference operator[] (size_type n) const {
        return m_array [n];
    }

    pointer       begin ()       { return m_array; }
    const_pointer begin () const { return m_array; }

    pointer       end ()       { return m_array + m_size; }
    const_pointer end () const { return m_array + m_size; }

    keys_type& keys () {
        return m_keys;
    }

    keys_type const& keys () const {
        return m_keys;
    }

    uniq_keys_type& uniq_keys () {
        return m_uniq_keys;
    }

    uniq_keys_type const& uniq_keys () const {
        return m_uniq_keys;
    }

    size_type key_count (key_type const& k) const {
        return m_keys.count (k);
    }

    size_type keys_count () const {
        return m_keys.size ();
    }

    size_type uniq_keys_count () const {
        return m_uniq_keys.size ();
    }

private:

    array (array const&);
    array& operator= (array const&);

    pointer m_array;
    size_type m_size;

    keys_type      m_keys;
    uniq_keys_type m_uniq_keys;

    generator_type m_generator;
    allocator_type m_allocator;
};

template< typename K, typename T, typename KeyOf, 
          typename G, typename C, typename A >
/* explicit */
array< K, T, KeyOf, G, C, A >::array (
    size_t n, G const& gen, C const& comp, A const& alloc)
    : m_array (), 
      m_size (n), 
      m_keys (comp), 
      m_uniq_keys (comp), 
      m_generator (gen), 
      m_allocator (alloc) 
{
    assert (m_size != 0);
    pointer p = m_allocator.allocate (m_size);

    size_type i = 0;
    try {
        for (; i < m_size; ++i) {
            value_type tmp = m_generator ();
            m_allocator.construct (p + i, tmp);

            key_type key = KeyOf ()(tmp);

            m_keys.insert (key);
            m_uniq_keys.insert (key);
        }
    }
    catch (...) {
        for (; i--;) m_allocator.destroy (p + i);
        m_allocator.deallocate (p, m_size);
        throw;
    }

    m_array = p;
}

template< typename K, typename T, typename KeyOf, 
          typename G, typename C, typename A >
array< K, T, KeyOf, G, C, A >::~array () 
{
    for (size_type i = m_size; i--;) 
        m_allocator.destroy (m_array + i);
    m_allocator.deallocate (m_array, m_size);
}

}}

#endif // CC_TESTS_DETAIL_BASIC_ARRAY_H
