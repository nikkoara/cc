// -*- C++ -*-

#ifndef CC_TESTS_DETAIL_PREDICATED_SORT_HPP
#define CC_TESTS_DETAIL_PREDICATED_SORT_HPP

#include <algorithm>
#include <functional>

namespace cc {
namespace test {

template< typename T, typename Comp, typename Pred >
struct sort {

    typedef T  value_type;
    typedef T* pointer;
    
    struct key_comp : std::binary_function< T, T, bool > {
        bool operator()(const T& x, const T& y) const {
            return Comp ()(Pred ()(x), Pred ()(y));
        }
    };

    template< typename Iterator >
    void operator() (Iterator beg, Iterator end) const {
        return std::sort (beg, end, key_comp ());
    }
};

}}

#endif // CC_TESTS_DETAIL_PREDICATED_SORT_HPP
