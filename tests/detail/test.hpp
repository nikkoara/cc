/* -*- mode: c++; mode: auto-fill; -*- */

#ifndef CC_TESTS_DETAIL_TEST_HPP
#define CC_TESTS_DETAIL_TEST_HPP

#include <cc/defs.hpp>

#include <cstdio>
#include <cstdlib>

struct unit_test_t {
    void pass_test () {
        ++total_tests;
    }

    void fail_test () {
        ++total_tests;
        ++failed_tests;

        if (failed_tests > 64 /* totally arbitrary */) {
            print ();
            exit (0);
        }
    }

    void warning () {
        ++total_tests;
        ++warnings;
    }

    unit_test_t (const char* s)
        : total_tests (0), failed_tests (0), warnings (0) {
        printf ("\n#\n# Test start - %s\n#\n", s);
    }

    virtual ~unit_test_t () {
        print ();
    }

private:
    void print () {
        printf ("\n# Total tests : %ld", total_tests);
        printf ("\n# Warnings    : %ld", warnings);
        printf ("\n# Failures    : %ld\n\n", failed_tests);
    }

    unit_test_t ();

private:
    long total_tests, failed_tests, warnings;
};

#define CC_TEST_DETAIL(t, expr, file, line)                         \
    {                                                               \
        if ((expr) == false) {                                      \
            (t).fail_test ();                                       \
            printf ("# Test failed: %s:%d\n\n#   ", file, line);    \
        }                                                           \
        else {                                                      \
            (t).pass_test ();                                       \
        }                                                           \
    }

#define CC_WARN_DETAIL(t, expr, file, line)                         \
   {                                                                \
        if ((expr) == false) {                                      \
            (t).warning ();                                         \
            printf ("# Test warning: %s:%d\n\n#   ", file, line);   \
        }                                                           \
        else {                                                      \
            (t).pass_test ();                                       \
        }                                                           \
   }

#define CC_TEST(t,expr) CC_TEST_DETAIL (t, expr, __FILE__, __LINE__)
#define CC_WARN(t,expr) CC_WARN_DETAIL (t, expr, __FILE__, __LINE__)

#define CC_NOTE(x) printf ("\n# Note: "); printf x; printf ("\n\n");
#define CC_STEP(x) printf ("\n# "); printf x; printf ("\n\n");

#endif // CC_TESTS_DETAIL_TEST_HPP
