// -*- mode: c++ -*-

#ifndef CC_TESTS_DETAIL_IDENT_COMPARE_HPP
#define CC_TESTS_DETAIL_IDENT_COMPARE_HPP

namespace cc {
namespace test {

template< typename T >
struct identity_comparator : public T
{
    identity_comparator () : instance (instance_counter++) { }

    identity_comparator (identity_comparator const& other)
        : instance (other.instance) { }

    identity_comparator& operator= (identity_comparator const& other) {
        instance = other.instance;
    }

    template< typename U >
    friend bool 
    operator== (identity_comparator< U > const&, identity_comparator< U > const&);

    template< typename U >
    friend bool
    operator!= (identity_comparator< U > const&, identity_comparator< U > const&);

private:
    
    int instance;
    static int instance_counter;
};

template< typename T >
/* static */ int identity_comparator< T >::instance_counter /* = 0 */;

template< typename T >
inline bool
operator== (identity_comparator< T > const& lhs, identity_comparator< T > const& rhs)
{
    return lhs.instance == rhs.instance;
}

template< typename T >
inline bool
operator!= (identity_comparator< T > const& lhs, identity_comparator< T > const& rhs)
{
    return !(lhs == rhs);
}

}}

#endif // CC_TESTS_DETAIL_IDENTITY_COMPARATOR_HPP
