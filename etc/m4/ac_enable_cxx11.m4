dnl -*- Autoconf -*-

AC_DEFUN([AC_ENABLE_CXX11],[
  CXXFLAGS+=' -std=c++11'
  AC_DEFINE([CC_HAVE_CXX11], [1], [Defined when using C++11])
])
