// -*- C++ -*-

#include <cc/defs.hpp>
#include <cc/detail/splay_tree.hpp>

#include <string.h>

namespace cc {
namespace detail {

template< typename K, typename T, typename R, typename C, typename A >
void splay_tree< K, T, R, C, A >::init ()
{
    m_end = (node_pointer)node_allocator ().allocate (1U);
    ::memset (m_end, 0, sizeof * m_end);

    m_end->child [0] = m_end->child [1] = m_end;
    m_end->p = m_end;
}

template< typename K, typename T, typename R, typename C, typename A >
/* explicit  */
splay_tree< K, T, R, C, A >::splay_tree (key_compare const& comp,
        allocator_type const& alloc)
    : A (alloc), m_end (), m_size (), m_comp (comp)
{
    init ();
}

template< typename K, typename T, typename R, typename C, typename A >
template< typename Iterator >
splay_tree< K, T, R, C, A >::splay_tree (Iterator first, Iterator last, bool dup,
        key_compare const& comp,
        allocator_type const& alloc)
    : A (alloc), m_end (), m_size (), m_comp (comp)
{
    init ();
    insert (first, last, dup);
}

template< typename K, typename T, typename R, typename C, typename A >
splay_tree< K, T, R, C, A >::splay_tree (splay_tree const& ref, bool dup)
    : A (ref), m_end (), m_size (), m_comp (ref.key_comp ())
{
    init ();
    insert (ref.begin (), ref.end (), dup);
}

template< typename K, typename T, typename R, typename C, typename A >
splay_tree< K, T, R, C, A >&
splay_tree< K, T, R, C, A >::operator= (splay_tree const& ref)
{
    if (this != &ref) {
        splay_tree< K, T, R, C, A > tmp (ref);
        swap (tmp);
    }

    return *this;
}

template< typename K, typename T, typename R, typename C, typename A >
splay_tree< K, T, R, C, A >::~splay_tree ()
{
    clear ();
    node_allocator ().deallocate (m_end, 1U);
}

template< typename K, typename T, typename R, typename C, typename A >
typename splay_tree< K, T, R, C, A >::iterator
splay_tree< K, T, R, C, A >::lower_bound (key_type const& k, bool do_splay)
{
    node_pointer y = m_end;

    for (node_pointer x = y->p; x && x != m_end;) {
        if (m_comp (x->key (), k)) {
            x = x->child [1];
        }
        else {
            y = x;
            x = x->child [0];
        }
    }

    if (do_splay) {
        splay (y);
    }

    return make_iter (y);
}

template< typename K, typename T, typename R, typename C, typename A >
typename splay_tree< K, T, R, C, A >::iterator
splay_tree< K, T, R, C, A >::upper_bound (key_type const& k, bool do_splay)
{
    node_pointer y = m_end;

    for (node_pointer x = y->p; x && x != m_end;) {
        if (m_comp (k, x->key ())) {
            y = x;
            x = x->child [0];
        }
        else {
            x = x->child [1];
        }
    }

    if (do_splay) {
        splay (y);
    }

    return make_iter (y);
}

template< typename K, typename T, typename R, typename C, typename A >
typename splay_tree< K, T, R, C, A >::iterator
splay_tree< K, T, R, C, A >::find (key_type const& k)
{
    if (empty ()) {
        return end ();
    }

    const iterator i = lower_bound (k, true);
    return i == end () || m_comp (k, i.m_node->key ()) ? end () : i;
}

template< typename K, typename T, typename R, typename C, typename A >
typename splay_tree< K, T, R, C, A >::node_pointer
splay_tree< K, T, R, C, A >::rotate_left (node_pointer p) throw ()
{
    assert (p && p != m_end);
    assert (p->child [1]);

    node_pointer parent = p;
    node_pointer child = parent->child [1];

    if (parent->p == m_end) {
        // Header's child is changing
        parent->p->p = child;
        child->p = parent->p;
    }
    else {
        if (parent == parent->p->child [0]) {
            parent->p->child [0] = child;
        }
        else if (parent == parent->p->child [1]) {
            parent->p->child [1] = child;
        }
        else {
            assert (false);
        }
    }

    if (child->child [0]) {
        child->child [0]->p = parent;
    }

    parent->child [1] = child->child [0];
    child->child [0] = parent;

    child->p = parent->p;
    parent->p = child;

    return child;
}

template< typename K, typename T, typename R, typename C, typename A >
typename splay_tree< K, T, R, C, A >::node_pointer
splay_tree< K, T, R, C, A >::rotate_right (node_pointer p) throw ()
{
    assert (p && p != m_end);
    assert (p->child [0]);

    node_pointer parent = p;
    node_pointer child = parent->child [0];

    if (parent->p == m_end) {
        // Header's child is changing
        parent->p->p = child;
        child->p = parent->p;
    }
    else {
        if (parent == parent->p->child [0]) {
            parent->p->child [0] = child;
        }
        else if (parent == parent->p->child [1]) {
            parent->p->child [1] = child;
        }
        else {
            assert (false);
        }
    }

    if (child->child [1]) {
        child->child [1]->p = parent;
    }

    parent->child [0] = child->child [1];
    child->child [1] = parent;

    child->p = parent->p;
    parent->p = child;

    return child;
}

template< typename K, typename T, typename R, typename C, typename A >
void
splay_tree< K, T, R, C, A >::splay (node_pointer p) throw ()
{
    // Splay while climbing up to the root
    for (; p && p != m_end && p->p != m_end; p = p->p) {
        if (p == p->p->child [0]) {
            if (p->p->p == m_end) {
                // one single rotation
                rotate_right (p->p);
                return;
            }

            if (p->p == p->p->p->child [0]) {
                // RR
                rotate_right (p->p->p);
                rotate_right (p->p);
            }
            else if (p->p == p->p->p->child [1]) {
                // LR
                rotate_right (p->p);
                rotate_left  (p->p);
            }
        }
        else if (p == p->p->child [1]) {
            if (p->p->p == m_end) {
                // one single rotation
                rotate_left (p->p);
                return;
            }

            if (p->p == p->p->p->child [0]) {
                // RL
                rotate_left  (p->p);
                rotate_right (p->p);
            }
            else if (p->p == p->p->p->child [1]) {
                // LL
                rotate_left (p->p->p);
                rotate_left (p->p);
            }
        }
    }
}

// Swap two nodes
template< typename K, typename T, typename R, typename C, typename A >
void splay_tree< K, T, R, C, A >::swap_nodes (node_pointer lhs, node_pointer rhs)
{
    assert (rhs);
    assert (lhs);

    assert (lhs->child [1] == 0);

    assert (lhs != m_end);
    assert (rhs != m_end);

    assert (lhs != rhs);

    // Update header links if pointing to them; lhs is always to the right of
    if (m_end->child [0] == lhs) {
        m_end->child [0] = rhs;
    }

    if (m_end->child [1] == rhs) {
        m_end->child [1] = lhs;
    }

    // Swap parents' links
    if (lhs->p == m_end) {
        int side = rhs->p->child [0] == rhs ? 0 : 1;
        std::swap (lhs, rhs->p->child [side]);
    }
    else if (rhs->p == m_end) {
        int side = lhs->p->child [0] == lhs ? 0 : 1;
        std::swap (rhs, lhs->p->child [side]);
    }
    else {
        int side [2] = { 0 };

        if (lhs->p->child [1] == lhs) {
            side [1] = 1;
        }

        if (rhs->p->child [1] == rhs) {
            side [0] = 1;
        }

        std::swap (lhs->p->child [side [1]], rhs->p->child [side [0]]);
    }

    // Swap parents
    std::swap (lhs->p, rhs->p);

    // Swap children links
    std::swap (lhs->child [0], rhs->child [0]);
    std::swap (lhs->child [1], rhs->child [1]);
}

/**************************************************************************/

template< typename K, typename T, typename R, typename C, typename A >
std::pair< typename splay_tree< K, T, R, C, A >::iterator, bool >
splay_tree< K, T, R, C, A >::insert (const_reference val, bool dup)
{
    node_pointer p = (node_pointer)node_allocator ().allocate (1);
    ::memset (p, 0, sizeof * p);

    try {
        this->construct (&p->value, val);
    }
    catch (...) {
        node_allocator ().deallocate (p, 1);
        throw;
    }

    // Shortcut: if no header, set this node as header
    if (m_end->p == m_end) {
        m_end->child [0] = m_end->child [1] = p;

        p->p = m_end;
        m_end->p = p;

        m_size = 1;

        return std::pair< iterator, bool > (make_iter (p), true);
    }

    // Otherwise, proceed to insert
    for (node_type* ptr = m_end->p;;) {

        int direction =
            m_comp (p->key (), ptr->key ()) ? -1 :
            m_comp (ptr->key (), p->key ()) ?  1 : 0;

        if (direction == 0 && !dup) {
            return std::pair< iterator, bool > (iterator (), false);
        }

        int side = direction > 0 ? 1 : 0;

        if (ptr->child [side]) {
            ptr = ptr->child [side];
        }
        else {
            ptr->child [side] = p;
            p->p = ptr;

            if (m_end->child [side] == ptr) {
                m_end->child [side] = p;
            }

            break;
        }
    }

    // splay node
    splay (p);

    // Adjust size
    ++m_size;

    return std::pair< iterator, bool > (make_iter (p), true);
}

// Erase element identified by iterator
template< typename K, typename T, typename R, typename C, typename A >
void
splay_tree< K, T, R, C, A >::erase (iterator pos)
{
    // Should never be the end
    if (pos == end ()) {
        return;
    }

    node_pointer p = pos.m_node;

    int mask = 0;

    if (p->child [0]) {
        mask |= 2;
    }

    if (p->child [1]) {
        mask |= 1;
    }

    switch (mask) {
    case 0:
        if (p->p == m_end) {
            m_end->p = m_end->child [0] = m_end->child [1] = m_end;
        }
        else {
            if (p->p->child [0] == p) {
                p->p->child [0] = 0;

                // This node could be a leftmost node
                if (m_end->child [0] == p) {
                    m_end->child [0] = p->p;
                }
            }
            else if (p->p->child [1] == p) {
                p->p->child [1] = 0;

                // May be a rightmost node
                if (m_end->child [1] == p) {
                    m_end->child [1] = p->p;
                }
            }
            else {
                assert (false);
            }
        }

        break;

    case 1:

        // This node gets replaced with its right child.
        if (p->p == m_end) {
            m_end->p = p->child [1];
            p->child [1]->p = m_end;

            // This node is a leftmost node
            m_end->child [0] = p->child [1];
        }
        else {
            if (p->p->child [0] == p) {
                p->child [1]->p = p->p;
                p->p->child [0] = p->child [1];

                // May be a leftmost node
                if (m_end->child [0] == p) {
                    m_end->child [0] = p->child [1];
                }
            }
            else if (p->p->child [1] == p) {
                p->child [1]->p = p->p;
                p->p->child [1] = p->child [1];
            }
            else {
                assert (false);
            }
        }

        break;

    case 2:

        // This node gets replaced with its left child.
        if (p->p == m_end) {
            m_end->p = p->child [0];
            p->child [0]->p = m_end;

            // This node is a rightmost node
            m_end->child [1] = p->child [0];
        }
        else {
            if (p->p->child [0] == p) {
                p->child [0]->p = p->p;
                p->p->child [0] = p->child [0];
            }
            else if (p->p->child [1] == p) {
                p->child [0]->p = p->p;
                p->p->child [1] = p->child [0];

                // May be a rightmost child
                if (m_end->child [1] == p) {
                    m_end->child [1] = p->child [0];
                }
            }
            else {
                assert (false);
            }
        }

        break;

    case 3:
        // This node gets replaced with rightmost child
        // of left subtree and then proceed to delete
        // that node.
        node_pointer ptr = node_type::rightmost (p->child [0]);
        swap_nodes (p, ptr);

        // After swap, node points to the bottom of the tree;
        // recursively call erase to erase this node.
        erase (make_iter (p));

        return;
    }

    this->destroy (&p->value);
    node_allocator ().deallocate (p, 1U);

    --m_size;
}

}}
