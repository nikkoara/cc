// -*- C++ -*-

#ifndef CC_DETAIL_KEY_REF_HPP
#define CC_DETAIL_KEY_REF_HPP

#include <utility>
#include <functional>

namespace cc {
namespace detail {

template< typename T, typename K = T >
struct key_ref : public std::unary_function< T, K > {
    typedef K key_type;
    typedef T value_type;

    key_type const& operator ()(value_type const& value) const {
        return value;
    }

    key_type& operator ()(value_type& value) const {
        return value;
    }
};

template< typename T, typename K >
struct key_ref< std::pair< K, T >, K >
        : public std::unary_function< std::pair< K, T >, K > {
    typedef K key_type;
    typedef std::pair< K, T > value_type;

    key_type const& operator ()(value_type const& value) const {
        return value.first;
    }

    key_type& operator ()(value_type& value) const {
        return value.first;
    }
};

template< typename T, typename K >
struct key_ref< std::pair< K const, T >, K >
        : public std::unary_function< std::pair< K const, T >, K > {
    typedef K key_type;
    typedef std::pair< K const, T > value_type;

    key_type const& operator () (value_type const& value) const {
        return value.first;
    }

    key_type const& operator ()(value_type& value) const {
        return value.first;
    }
};

}}

#endif // CC_DETAIL_KEY_REF_HPP
