// -*- C++ -*-

#ifndef CC_DETAIL_HB_TREE_HPP
#define CC_DETAIL_HB_TREE_HPP

#include <cc/defs.hpp>
#include <cc/detail/key_ref.hpp>

#include <memory>
#include <functional>
#include <iterator>

namespace cc {
namespace detail {

template< typename Alloc, typename T, typename K, typename R >
struct hb_tree_node {
    typedef int     int_type;
    typedef size_t  size_type;

    typedef K key_type;
    typedef T value_type;

    typedef T&        reference;
    typedef T const&  const_reference;

    typedef R key_reference;

    typedef Alloc allocator_type;

    typedef CC_REBIND(allocator_type, hb_tree_node) node_allocator;
    typedef typename node_allocator::pointer node_pointer;

    typedef CC_REBIND(allocator_type, K) key_allocator;
    typedef typename key_allocator::const_reference key_const_reference;

    node_pointer  p, child [2];
    size_type     distance;
    value_type    value;

    key_const_reference key () const {
        return key_reference ()(value);
    }

    static node_pointer most (node_pointer p, int side) throw () {
        assert (p);

        for (; p->child [side]; p = p->child [side]) ;

        return p;
    }

    static node_pointer leftmost (node_pointer p) throw () {
        return most (p, 0);
    }

    static node_pointer rightmost (node_pointer p) throw () {
        return most (p, 1);
    }
};

template< typename T, typename Diff, typename Ptr, typename Ref, typename Node >
struct hb_tree_iterator
        : public std::iterator< std::bidirectional_iterator_tag, T, Diff, Ptr, Ref > {
protected:

    typedef
    std::iterator< std::bidirectional_iterator_tag, T, Diff, Ptr, Ref >
    base_type;

public:

    typedef typename base_type::value_type        value_type;
    typedef typename base_type::pointer           pointer;
    typedef typename base_type::reference         reference;
    typedef typename base_type::difference_type   difference_type;
    typedef typename base_type::iterator_category iterator_category;

    typedef Node node_type;

    typedef typename node_type::allocator_type allocator_type;
    typedef typename node_type::node_pointer   node_pointer;

    typedef typename allocator_type::size_type size_type;

    typedef const value_type* const_pointer;
    typedef const value_type& const_reference;

    node_pointer m_node, m_end;

    hb_tree_iterator() : m_node (), m_end () { }

    hb_tree_iterator(node_pointer pnode, node_pointer pend)
        : m_node (pnode), m_end (pend) { }

    hb_tree_iterator(hb_tree_iterator const& iter)
        : m_node (iter.m_node), m_end (iter.m_end) { }

    template< typename P, typename R >
    hb_tree_iterator (hb_tree_iterator< T, Diff, P, R, Node > const& ref)
        : m_node (ref.m_node), m_end (ref.m_end) {
    }

    hb_tree_iterator& operator=(const hb_tree_iterator& ref) {
        if (this != &ref) {
            m_node = ref.m_node;
            m_end  = ref.m_end;
        }

        return *this;
    }

    hb_tree_iterator& operator++ () {
        if (m_node->child [1]) {
            assert(m_node != m_end);
            m_node = node_type::leftmost (m_node->child [1]);
        }
        else {
            for (; m_node->p->child [1] == m_node; m_node = m_node->p) ;

            if (m_node != m_end) {
                m_node = m_node->p;
            }
        }

        return *this;
    }

    hb_tree_iterator  operator++ (int) {
        hb_tree_iterator tmp (*this);
        return ++*this, tmp;
    }

    hb_tree_iterator& operator-- () {
        if (m_node->child [0]) {
            if (m_node == m_end) {
                m_node = m_node->child [1];
            }
            else {
                m_node = node_type::rightmost (m_node->child [0]);
            }
        }
        else {
            for (; m_node->p->child [0] == m_node; m_node = m_node->p) ;

            m_node = m_node->p;
        }

        return *this;
    }

    hb_tree_iterator operator-- (int) {
        hb_tree_iterator tmp (*this);
        return --*this, tmp;
    }

    reference operator* () const {
        return m_node->value;
    }

    pointer operator-> () const {
        return &** this;
    }
};

template <typename T, typename Diff, typename Ptr, typename Ref, typename Node >
inline bool
operator== (hb_tree_iterator< T, Diff, Ptr, Ref, Node > const& lhs,
            hb_tree_iterator< T, Diff, Ptr, Ref, Node > const& rhs)
{
    return lhs.m_node == rhs.m_node && lhs.m_end == rhs.m_end;
}

template <typename T, typename Diff, typename Ptr, typename Ref, typename Node >
inline bool
operator!= (hb_tree_iterator< T, Diff, Ptr, Ref, Node > const& lhs,
            hb_tree_iterator< T, Diff, Ptr, Ref, Node > const& rhs)
{
    return !(lhs == rhs);
}

template< typename K, typename T,
          typename R = key_ref< T, K >,
          typename Comp = std::less< K >,
          typename Alloc = std::allocator< T > >
struct hb_tree : private Alloc {
    typedef hb_tree_node< Alloc, T, K, R > node_type;

    typedef CC_REBIND (Alloc, T)         value_allocator;
    typedef CC_REBIND (Alloc, K)         key_allocator;
    typedef CC_REBIND (Alloc, node_type) node_allocator;

    typedef typename node_allocator::pointer node_pointer;

public:

    typedef hb_tree tree_type;

    typedef int int_type;

    typedef K  key_type;
    typedef T  value_type;

    typedef R  key_reference;

    typedef Comp   key_compare;
    typedef Alloc  allocator_type;

    typedef typename value_allocator::pointer          pointer;
    typedef typename value_allocator::const_pointer    const_pointer;

    typedef typename value_allocator::reference        reference;
    typedef typename value_allocator::const_reference  const_reference;

    typedef typename allocator_type::size_type         size_type;
    typedef typename allocator_type::difference_type   difference_type;

    typedef hb_tree_iterator <
    value_type, difference_type, pointer, reference, node_type >
    tree_iter;

    typedef hb_tree_iterator <
    value_type, difference_type, const_pointer, const_reference, node_type >
    tree_citer;

    typedef tree_iter   iterator;
    typedef tree_citer  const_iterator;

    typedef std::reverse_iterator< iterator > reverse_iterator;
    typedef std::reverse_iterator< const_iterator > const_reverse_iterator;

    // Iterator utilities
    iterator make_iter (node_pointer p) {
        return iterator (p, m_end);
    }

    const_iterator make_iter (node_pointer p) const {
        return const_iterator (p, m_end);
    }

    explicit
    hb_tree (int = 2,
             key_compare const& = key_compare (),
             allocator_type const& = allocator_type ());

    template< typename Iterator >
    hb_tree (Iterator, Iterator, bool = false, int = 2,
             key_compare const& = key_compare (),
             allocator_type const& = allocator_type ());

    hb_tree (hb_tree const&, bool = false);

    hb_tree& operator= (hb_tree const&);

    ~hb_tree ();

    key_compare key_comp () const {
        return m_comp;
    }

    value_allocator get_allocator () const {
        return value_allocator (*this);
    }

    int balance () const {
        return m_balance;
    }

    iterator begin () {
        return make_iter (m_end->child [0]);
    }

    iterator end () {
        return make_iter (m_end);
    }

    const_iterator begin () const {
        return make_iter (m_end->child [0]);
    }

    const_iterator end () const {
        return make_iter (m_end);
    }

    reverse_iterator rbegin () {
        return reverse_iterator (end ());
    }

    reverse_iterator rend () {
        return reverse_iterator (begin ());
    }

    const_reverse_iterator rbegin () const {
        return const_reverse_iterator (end ());
    }

    const_reverse_iterator rend () const {
        return const_reverse_iterator (begin ());
    }

    std::pair< iterator, bool > insert (const_reference, bool);

    std::pair< iterator, bool >
    insert (iterator /* ignore */, const_reference value, bool dup) {
        return insert(value, dup);
    }

    template< typename Iterator >
    void insert (Iterator beg_iter, Iterator end_iter, bool dup) {
        for (; beg_iter != end_iter; ++beg_iter) {
            insert (*beg_iter, dup);
        }
    }

    void erase (iterator);

    void erase (iterator first, iterator last) {
        for (; first != last;) {
            erase (first++);
        }
    }

    size_type erase (key_type const& k) {
        std::pair< iterator, iterator > interval = equal_range (k);

        if (interval.first == end ()) {
            return 0U;
        }

        size_type n = std::distance (interval.first, interval.second);
        erase (interval.first, interval.second);

        return n;
    }

    void clear () {
        erase (begin (), end ());
    }

    iterator find (const key_type&);

    const_iterator find (const key_type& k) const {
        return const_cast< hb_tree* > (this)->find (k);
    }

    iterator lower_bound (key_type const&);

    const_iterator lower_bound (key_type const& k) const {
        return const_cast< hb_tree* >(this)->lower_bound (k);
    }

    iterator upper_bound (key_type const&);

    const_iterator upper_bound (key_type const& k) const {
        return const_cast< hb_tree* >(this)->upper_bound (k);
    }

    std::pair< iterator, iterator >
    equal_range (key_type const& k) {
        typedef std::pair< iterator, iterator > pair_type;
        return pair_type (lower_bound (k), upper_bound (k));
    }

    std::pair< const_iterator, const_iterator >
    equal_range (key_type const& k) const {
        return const_cast< tree_type* > (this)->equal_range (k);
    }

    void swap (tree_type& ref) {
        if (this == &ref) {
            return;
        }

        if (get_allocator () == ref.get_allocator ()) {
            std::swap (m_end,     ref.m_end);
            std::swap (m_size,    ref.m_size);
            std::swap (m_balance, ref.m_balance);
            std::swap (m_comp,    ref.m_comp);
        }
        else {
            tree_type tmp = *this;
            *this = ref;
            ref = tmp;
        }
    }

    size_type size () const {
        return m_size;
    }

    size_type empty () const {
        return size () == 0;
    }

    size_type count (key_type const& k) const {
        std::pair< const_iterator, const_iterator > p = equal_range (k);
        return std::distance (p.first, p.second);
    }

#if defined (CC_UNIT_TEST)
    node_pointer tree_header () {
        return m_end;
    }
#endif // CC_UNIT_TEST

protected:

    void init ();

    void restore_property (node_pointer);
    void swap_nodes (node_pointer, node_pointer);

    size_type max_distance (node_pointer);
    int_type weigh (node_pointer);

    node_pointer rotate_left (node_pointer);
    node_pointer rotate_right (node_pointer);

private:

    node_pointer m_end;
    size_t m_size;
    int m_balance;
    key_compare m_comp;
};

}}

#  include "hb_tree.cc"

#endif // CC_DETAIL_HB_TREE_HPP
