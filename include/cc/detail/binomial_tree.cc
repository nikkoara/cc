// -*- C++ -*-

#include <cc/detail/defs.hpp>
#include <cc/detail/binomial_tree.hpp>

#include <stdexcept>
#include <cstring>

namespace cc {
namespace detail {

template< typename K, typename T, typename Off,
          typename R, typename C, typename A >
inline void
binomial_tree< K, T, Off, R, C, A >::init ()
{
    m_end = (node_pointer)node_allocator ().allocate (1U);
    ::memset (m_end, 0, sizeof * m_end);
    m_end->p = m_end->child [0] = m_end;
}

template< typename K, typename T, typename Off,
          typename R, typename C, typename A >
template< typename Iterator >
inline void
binomial_tree< K, T, Off, R, C, A >::insert (Iterator first, Iterator last)
{
    for (; first != last; ++first) {
        push (*first);
    }
}

template< typename K, typename T, typename Off,
          typename R, typename C, typename A >
inline void
binomial_tree< K, T, Off, R, C, A >::clear ()
{
    node_pointer p = m_end->child [0];

    if (p == m_end) {
        return;
    }

    for (; p != m_end;) {
        if (p->child [0]) {
            p = p->child [0];
        }
        else if (p->child [1]) {
            p = p->child [1];
        }
        else {
            if (p->p->child [0] == p) {
                p->p->child [0] = 0;
            }
            else {
                p->p->child [1] = 0;
            }

            node_pointer save = p->p;

            this->destroy (&p->value);
            node_allocator ().deallocate (p, 1U);

            p = save;
        }
    }

    m_size = 0;
    m_end->child [0] = 0;
}

template< typename K, typename T, typename Off,
          typename R, typename C, typename A >
/* explicit  */
binomial_tree< K, T, Off, R, C, A >::binomial_tree (
    key_compare const& comp, allocator_type const& alloc)
    : A (alloc), m_end (), m_size (), m_comp (comp)
{
    init ();
}

template< typename K, typename T, typename Off,
          typename R, typename C, typename A >
template< typename Iterator >
binomial_tree< K, T, Off, R, C, A >::binomial_tree (
    Iterator first, Iterator last,
    key_compare const& comp, allocator_type const& alloc)
    : A (alloc), m_end (), m_size (), m_comp (comp)
{
    init ();
    insert (first, last);
}

template< typename K, typename T, typename Off,
          typename R, typename C, typename A >
binomial_tree< K, T, Off, R, C, A >::binomial_tree (binomial_tree const& ref)
    : A (ref), m_end (), m_size (), m_comp (ref.key_comp ())
{
    init ();
    insert (ref.begin (), ref.end ());
}

template< typename K, typename T, typename Off,
          typename R, typename C, typename A >
binomial_tree< K, T, Off, R, C, A >&
binomial_tree< K, T, Off, R, C, A >::operator= (binomial_tree const& ref)
{
    if (this != &ref) {
        clear ();
        insert (ref.begin (), ref.end ());
    }

    return *this;
}

template< typename K, typename T, typename Off,
          typename R, typename C, typename A >
binomial_tree< K, T, Off, R, C, A >::~binomial_tree ()
{
    clear ();
    node_allocator ().deallocate (m_end, 1U);
}

/**************************************************************************/

template< typename K, typename T, typename Off,
          typename R, typename C, typename A >
typename binomial_tree< K, T, Off, R, C, A >::node_pointer
binomial_tree< K, T, Off, R, C, A >::restore_property (node_pointer p)
{
    if (p == 0) {
        return 0;
    }

    node_pointer a = 0, b = p, c = p->child [1];

    for (; c ;) {
        if (b->degree != c->degree ||
                (c->child [1] && c->child [1]->degree == c->degree)) {
            a = b;
            b = c;
            c = c->child [1];
        }
        else if (m_comp (b->key (), c->key ())) {
            // Merge c under b

            b->child [1] = c->child [1];

            if (c->child [1]) {
                c->child [1]->p = b;
                c->child [1] = 0;
            }

            if (b->child [0]) {
                for (p = b->child [0]; p->child [1]; p = p->child [1]) ;

                p->child [1] = c;
                c->p = p;
            }
            else {
                b->child [0] = c;
                c->p = b;
            }

            c = b->child [1];
            ++b->degree;
        }
        else {
            // Merge b under c

            c->p = a;

            if (a) {
                a->child [1] = c;
            }

            if (c->child [0]) {
                for (p = c->child [0]; p->child [1]; p = p->child [1]) ;

                p->child [1] = b;
                b->p = p;
            }
            else {
                c->child [0] = b;
                b->p = c;
            }

            b->child [1] = 0;

            b = c;
            c = c->child [1];

            ++b->degree;
        }
    }

    for (p = b; p->p; p = p->p) ;

    return p;
}

template< typename K, typename T, typename Off,
          typename R, typename C, typename A >
typename binomial_tree< K, T, Off, R, C, A >::node_pointer
binomial_tree< K, T, Off, R, C, A >::merge (node_pointer lhs, node_pointer rhs)
{
    node_pointer root = 0, ptr = 0;

    if (lhs == 0) {
        return rhs;
    }

    if (rhs == 0) {
        return lhs;
    }

    lhs->p = rhs->p = 0;

    if (lhs->degree < rhs->degree) {
        ptr = lhs;
        lhs = lhs->child [1];
    }
    else {
        ptr = rhs;
        rhs = rhs->child [1];
    }

    root = ptr;
    ptr->p = ptr->child [1] = 0;

    for (;;) {

        if (lhs == 0) {
            ptr->child [1] = rhs;
            rhs && (rhs->p = ptr);
            break;
        }

        if (rhs == 0) {
            ptr->child [1] = lhs;
            lhs->p = ptr;
            break;
        }

        if (lhs->degree < rhs->degree) {
            ptr->child [1] = lhs;
            lhs->p = ptr;

            lhs = lhs->child [1];
            lhs && (lhs->p = 0, true);
        }
        else {
            ptr->child [1] = rhs;
            rhs->p = ptr;

            rhs = rhs->child [1];
            rhs && (rhs->p = 0, true);
        }

        // Advance node
        ptr = ptr->child [1];
        ptr->p = ptr->child [1] = 0;
    }

    return restore_property (root);
}

/**************************************************************************/

template< typename K, typename T, typename Off,
          typename R, typename C, typename A >
typename binomial_tree< K, T, Off, R, C, A >::iterator
binomial_tree< K, T, Off, R, C, A >::top ()
{
    if (empty ()) {
        return make_iter (m_end);
    }

    node_pointer p, pp;
    p = pp = m_end->child [0];

    for (; p; p = p->child [1]) {
        assert (p->child [1] == 0 || p->degree < p->child [1]->degree);

        if (m_comp (p->key (), pp->key ())) {
            pp = p;
        }
    }

    return make_iter (pp);
}

template< typename K, typename T, typename Off,
          typename R, typename C, typename A >
typename binomial_tree< K, T, Off, R, C, A >::const_iterator
binomial_tree< K, T, Off, R, C, A >::top () const
{
    return const_cast< binomial_tree* >(this)->top ();
}

template< typename K, typename T, typename Off,
          typename R, typename C, typename A >
typename binomial_tree< K, T, Off, R, C, A >::iterator
binomial_tree< K, T, Off, R, C, A >::push (value_type const& val)
{
    node_pointer p = (node_pointer)node_allocator ().allocate (1U);
    ::memset (p, 0, sizeof * p);

    try {
        this->construct (&p->value, val);
    }
    catch (...) {
        node_allocator ().deallocate (p, 1);
        throw;
    }

    p->degree = 0;

    if (!empty ()) {
        p = merge (m_end->child [0], p);
    }

    m_end->child [0] = p;
    p&&  (p->p = m_end);

    return ++m_size, make_iter (p);
}

template< typename K, typename T, typename Off,
          typename R, typename C, typename A >
void
binomial_tree< K, T, Off, R, C, A >::pop ()
{
    if (empty ()) {
        return;
    }

    iterator iter = top ();

    node_pointer p = iter.m_node, h = m_end->child [0], hh = p->child [0];
    h->p = p->child [0] = 0;

    if (hh) {
        hh->p = 0;
    }

    if (h == p) {
        h = p->child [1];
    }
    else {
        p->p->child [1] = p->child [1];
        p->child [1]&&  (p->child [1]->p = p->p);
    }

    this->destroy (&p->value);
    node_allocator ().deallocate (p, 1);

    --m_size;

    p = merge (h, hh);

    if (p) {
        m_end->child [0] = p;
        p->p = m_end;
    }
    else {
        m_end->child [0] = m_end;
    }
}

template< typename K, typename T, typename Off,
          typename R, typename C, typename A >
void
binomial_tree< K, T, Off, R, C, A >::merge (tree_type& ref)
{
    node_pointer p, lhs, rhs;

    lhs =     empty () ? 0 :     m_end->child [0];
    rhs = ref.empty () ? 0 : ref.m_end->child [0];

    p = merge (lhs, rhs);

    ref.m_size = 0;
    ref.m_end->child [0] = ref.m_end;

    m_size += ref.m_size;

    m_end->child [0] = p ? p : m_end;
    p&&  (p->p = m_end);
}

template< typename K, typename T, typename Off,
          typename R, typename C, typename A >
void
binomial_tree< K, T, Off, R, C, A >::change_key (
    iterator iter, offset_type const& off)
{
    assert (iter != end ());

    node_pointer p = iter.m_node;
    assert (!m_comp (p->key (), p->key () + off));

    p->key (p->key () + off);

    for (node_pointer pp = p->binomial_parent ();
            pp != m_end; pp = p->binomial_parent ()) {

        if (p == p->p->child [0]) {
            if (pp == pp->p->child [0]) {
                pp->p->child [0] = p;
            }
            else {
                pp->p->child [1] = p;
            }

            pp->child [1]&&  (pp->child [1]->p = p);

            p->child [0]&&  (p->child [0]->p = pp);
            p->child [1]&&  (p->child [1]->p = pp);

            p->p = pp->p;
            pp->p = p;

            pp->child [0] = p->child [0];
            p ->child [0] = pp;

            std::swap (p->child [1], pp->child [1]);
        }
        else {
            if (pp == pp->p->child [0]) {
                pp->p->child [0] = pp->child [0]->p = p;
            }
            else {
                pp->p->child [1] = pp->child [0]->p = p;
            }

            pp->child [1]&&  (pp->child [1]->p = p);

            p->p->child [1] = pp;
            p->child [0]&&  (p->child [0]->p = pp);
            p->child [1]&&  (p->child [1]->p = pp);

            std::swap (p->p,         pp->p);
            std::swap (p->child [0], pp->child [0]);
            std::swap (p->child [1], pp->child [1]);
        }
    }
}

}
}
