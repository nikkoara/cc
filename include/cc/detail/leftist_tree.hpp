// -*- mode: C++ -*-

#ifndef CC_DETAIL_LEFTIST_TREE_HPP
#define CC_DETAIL_LEFTIST_TREE_HPP

#include <cc/defs.hpp>
#include <cc/detail/key_ref.hpp>

namespace cc {
namespace detail {

template< typename Alloc, typename T, typename K, typename R >
struct leftist_tree_node {
    typedef int     int_type;
    typedef size_t  size_type;

    typedef K key_type;
    typedef T value_type;

    typedef R  key_reference;

    typedef T& reference;
    typedef T const&  const_reference;

    typedef Alloc allocator_type;
    typedef CC_REBIND(allocator_type, leftist_tree_node) node_allocator;

    typedef typename node_allocator::pointer node_pointer;

    typedef CC_REBIND(allocator_type, K) key_allocator;
    typedef typename key_allocator::const_reference key_const_reference;

    node_pointer  p, child [2];
    size_type     distance;
    value_type    value;

    key_const_reference key () const {
        return R ()(value);
    }

    void key (key_const_reference k) {
        R ()(value) = k;
    }

    node_pointer rightmost () {
        node_pointer prev, pcur;

        for (prev = pcur = this; pcur;
                prev = pcur,
                pcur = pcur->child [1] ? pcur->child [1] : pcur->child [0]) ;

        return prev;
    }
};

template< typename T, typename Diff, typename Ptr, typename Ref, typename Node >
struct leftist_tree_iterator
        : public std::iterator< std::random_access_iterator_tag, T, Diff, Ptr, Ref > {
protected:

    typedef
    std::iterator< std::random_access_iterator_tag, T, Diff, Ptr, Ref >
    base_type;

public:

    typedef typename base_type::value_type        value_type;
    typedef typename base_type::pointer           pointer;
    typedef typename base_type::reference         reference;
    typedef typename base_type::difference_type   difference_type;
    typedef typename base_type::iterator_category iterator_category;

    typedef Node node_type;

    typedef typename node_type::allocator_type allocator_type;
    typedef typename node_type::node_pointer   node_pointer;

    typedef typename allocator_type::size_type size_type;

    typedef const value_type* const_pointer;
    typedef const value_type& const_reference;

    node_pointer m_node, m_end;

    leftist_tree_iterator() : m_node (), m_end () { }

    leftist_tree_iterator(node_pointer pnode, node_pointer pend)
        : m_node (pnode), m_end (pend) { }

    leftist_tree_iterator(leftist_tree_iterator const& iter)
        : m_node (iter.m_node), m_end (iter.m_end) { }

    template< typename P, typename R >
    leftist_tree_iterator (leftist_tree_iterator< T, Diff, P, R, Node > const& ref)
        : m_node (ref.m_node), m_end (ref.m_end) {
    }

    leftist_tree_iterator& operator=(const leftist_tree_iterator& ref) {
        if (this != &ref) {
            m_node = ref.m_node;
            m_end  = ref.m_end;
        }

        return *this;
    }

    leftist_tree_iterator& operator++ () {
        assert (m_node && m_end);
        assert (m_node != m_end);

        if (m_node->child [0]) {
            m_node = m_node->child [0];
        }
        else if (m_node->child [1]) {
            m_node = m_node->child [1];
        }
        else {
            for (; m_node != m_end &&
                    (m_node->p->child [1] == 0 || m_node->p->child [1] == m_node);
                    m_node = m_node->p) ;

            if (m_node != m_end) {
                m_node = m_node->p->child [1];
            }
        }

        return *this;
    }

    leftist_tree_iterator operator++ (int) {
        leftist_tree_iterator tmp (*this);
        return ++*this, tmp;
    }

    leftist_tree_iterator& operator-- () {
        assert (m_node && m_end);
        assert (m_end == m_end->p || m_node != m_end->p);

        if (m_node == m_node->p->child [0]) {
            m_node = m_node->p;
        }
        else if (m_node == m_node->p->child [1]) {
            m_node = m_node->p;

            if (m_node->child [0]) {
                m_node = m_node->child [0]->rightmost ();
            }
        }
        else {
            m_node = m_node->child [0]->rightmost ();
        }

        return *this;
    }

    leftist_tree_iterator operator-- (int) {
        leftist_tree_iterator tmp (*this);
        return --*this, tmp;
    }

    reference operator* () const {
        return m_node->value;
    }

    pointer operator-> () const {
        return &** this;
    }
};

template <typename T, typename Diff, typename Ptr, typename Ref, typename Node >
inline bool
operator== (leftist_tree_iterator< T, Diff, Ptr, Ref, Node > const& lhs,
            leftist_tree_iterator< T, Diff, Ptr, Ref, Node > const& rhs)
{
    return lhs.m_node == rhs.m_node && lhs.m_end == rhs.m_end;
}

template <typename T, typename Diff, typename Ptr, typename Ref, typename Node >
inline bool
operator!= (leftist_tree_iterator< T, Diff, Ptr, Ref, Node > const& lhs,
            leftist_tree_iterator< T, Diff, Ptr, Ref, Node > const& rhs)
{
    return !(lhs == rhs);
}

template< typename K, typename T, typename Off = K,
          typename R = key_ref< T, K >,
          typename Comp = std::less< K >,
          typename Alloc = std::allocator< T > >
struct leftist_tree : private Alloc {
    typedef leftist_tree_node< Alloc, T, K, R > node_type;

    typedef CC_REBIND (Alloc, T)         value_allocator;
    typedef CC_REBIND (Alloc, K)         key_allocator;
    typedef CC_REBIND (Alloc, node_type) node_allocator;

    typedef typename node_allocator::pointer node_pointer;

public:

    typedef leftist_tree tree_type;

    typedef int int_type;

    typedef K  key_type;
    typedef T  value_type;

    typedef R  key_reference;
    typedef Off offset_type;

    typedef Comp   key_compare;
    typedef Alloc  allocator_type;

    typedef typename value_allocator::pointer          pointer;
    typedef typename value_allocator::const_pointer    const_pointer;

    typedef typename value_allocator::reference        reference;
    typedef typename value_allocator::const_reference  const_reference;

    typedef typename allocator_type::size_type         size_type;
    typedef typename allocator_type::difference_type   difference_type;

    typedef leftist_tree_iterator <
    value_type, difference_type, pointer, reference, node_type >
    tree_iter;

    typedef leftist_tree_iterator <
    value_type, difference_type, const_pointer, const_reference, node_type >
    tree_citer;

    typedef tree_iter   iterator;
    typedef tree_citer  const_iterator;

    typedef std::reverse_iterator< iterator > reverse_iterator;
    typedef std::reverse_iterator< const_iterator > const_reverse_iterator;

protected:

    // Iterator utilities
    iterator make_iter (node_pointer p = 0) {
        return iterator (p, m_end);
    }

    const_iterator make_iter (node_pointer p = 0) const {
        return const_iterator (p, m_end);
    }

public:

    explicit
    leftist_tree (key_compare const& = key_compare (),
                  allocator_type const& = allocator_type ());

    template< typename Iterator >
    leftist_tree (Iterator, Iterator,
                  key_compare const& = key_compare (),
                  allocator_type const& = allocator_type ());

    leftist_tree (leftist_tree const&);

    leftist_tree& operator= (leftist_tree const&);

    ~leftist_tree ();

    key_compare key_comp () const {
        return m_comp;
    }

    value_allocator get_allocator () const {
        return value_allocator (*this);
    }

    iterator begin () {
        return make_iter (m_end->child [0]);
    }

    iterator end () {
        return make_iter (m_end);
    }

    const_iterator begin () const {
        return make_iter (m_end->child [0]);
    }

    const_iterator end () const {
        return make_iter (m_end);
    }

    reverse_iterator rbegin () {
        return reverse_iterator (end ());
    }

    reverse_iterator rend () {
        return reverse_iterator (begin ());
    }

    const_reverse_iterator rbegin () const {
        return const_reverse_iterator (end ());
    }

    const_reverse_iterator rend () const {
        return const_reverse_iterator (begin ());
    }

    iterator top ();

    const_iterator top () const;

    iterator push (value_type const&);

    void pop ();

    void merge (leftist_tree&);

    void change_key (iterator, offset_type const&);

    size_type size () const {
        return m_size;
    }

    bool empty () const {
        return m_size == 0;
    }

    void clear ();

    void swap (tree_type& ref) {
        if (this == &ref) {
            return;
        }

        if (get_allocator () == ref.get_allocator ()) {
            std::swap (m_end,  ref.m_end);
            std::swap (m_size, ref.m_size);
            std::swap (m_comp, ref.m_comp);
        }
        else {
            tree_type tmp = *this;
            *this = ref;
            ref = tmp;
        }
    }

#if defined (CC_UNIT_TEST)
    node_pointer tree_header () {
        return m_end;
    }
#endif // CC_UNIT_TEST

protected:

    void init ();

    void restore_property (node_pointer);
    node_pointer merge (node_pointer, node_pointer);

    template< typename Iterator >
    void insert (Iterator, Iterator);

protected:

    node_pointer m_end;
    size_t m_size;
    key_compare m_comp;
};

}}

#  include "leftist_tree.cc"

#endif // CC_DETAIL_LEFTIST_TREE_HPP
