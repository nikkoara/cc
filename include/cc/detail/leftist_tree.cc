// -*- C++ -*-

#include <cc/defs.hpp>
#include <cc/detail/leftist_tree.hpp>

#include <stdexcept>
#include <cstring>

namespace cc {
namespace detail {

template< typename K, typename T, typename Off,
          typename R, typename C, typename A >
inline void
leftist_tree< K, T, Off, R, C, A >::init ()
{
    m_end = (node_pointer)node_allocator ().allocate (1U);
    ::memset (m_end, 0, sizeof * m_end);
    m_end->p = m_end->child [0] = m_end->child [1] = m_end;
}

template< typename K, typename T, typename Off,
          typename R, typename C, typename A >
template< typename Iterator >
inline void
leftist_tree< K, T, Off, R, C, A >::insert (Iterator first, Iterator last)
{
    for (; first != last; ++first) {
        push (*first);
    }
}

template< typename K, typename T, typename Off,
          typename R, typename C, typename A >
inline void
leftist_tree< K, T, Off, R, C, A >::clear ()
{
    for (node_pointer p = m_end->child [0]; p != m_end;) {
        if (p->child [0]) {
            p = p->child [0];
            continue;
        }
        else if (p->child [1]) {
            p = p->child [1];
            continue;
        }
        else {
            if (p->p->child [0] == p) {
                p->p->child [0] = 0;
            }
            else {
                p->p->child [1] = 0;
            }

            node_pointer save = p->p;

            this->destroy (&p->value);
            node_allocator ().deallocate (p, 1U);

            p = save;
        }
    }

    m_size = 0;
    m_end->p = m_end->child [0] = m_end->child [1] = m_end;
}

template< typename K, typename T, typename Off,
          typename R, typename C, typename A >
/* explicit  */
leftist_tree< K, T, Off, R, C, A >::leftist_tree (key_compare const& comp,
        allocator_type const& alloc)
    : A (alloc), m_end (), m_size (), m_comp (comp)
{
    init ();
}

template< typename K, typename T, typename Off,
          typename R, typename C, typename A >
template< typename Iterator >
leftist_tree< K, T, Off, R, C, A >::leftist_tree (Iterator first, Iterator last,
        key_compare const& comp,
        allocator_type const& alloc)
    : A (alloc), m_end (), m_size (), m_comp (comp)
{
    init ();
    insert (first, last);
}

template< typename K, typename T, typename Off,
          typename R, typename C, typename A >
leftist_tree< K, T, Off, R, C, A >::leftist_tree (leftist_tree const& ref)
    : A (ref), m_end (), m_size (), m_comp (ref.key_comp ())
{
    init ();
    insert (ref.begin (), ref.end ());
}

template< typename K, typename T, typename Off,
          typename R, typename C, typename A >
leftist_tree< K, T, Off, R, C, A >&
leftist_tree< K, T, Off, R, C, A >::operator= (leftist_tree const& ref)
{
    if (this != &ref) {
        clear ();
        insert (ref.begin (), ref.end ());
    }

    return *this;
}

template< typename K, typename T, typename Off,
          typename R, typename C, typename A >
leftist_tree< K, T, Off, R, C, A >::~leftist_tree ()
{
    clear ();
    node_allocator ().deallocate (m_end, 1U);
}

/**************************************************************************/

template< typename K, typename T, typename Off,
          typename R, typename C, typename A >
void
leftist_tree< K, T, Off, R, C, A >::restore_property (node_pointer p)
{
    for (; p; p = p->p) {
        if (p->child [1]) {
            if (p->child [0] == 0 ||
                    p->child [1]->distance < p->child [0]->distance) {
                std::swap (p->child [0], p->child [1]);
            }
        }

        p->distance = p->child [1] ? p->child [1]->distance + 1 : 1;
    }
}

template< typename K, typename T, typename Off,
          typename R, typename C, typename A >
typename leftist_tree< K, T, Off, R, C, A >::node_pointer
leftist_tree< K, T, Off, R, C, A >::merge (node_pointer lhs, node_pointer rhs)
{
    // Consider the case where one or both heaps are empty
    if (lhs == 0) {
        return rhs;
    }

    if (rhs == 0) {
        return lhs;
    }

    rhs->p = lhs->p = 0;

    node_pointer ptr = m_comp (rhs->key (), lhs->key ()) ? rhs : lhs;

    for (;;) {
        if (m_comp (rhs->key (), lhs->key ())) {
            node_pointer save = lhs->p;

            if (save) {
                std::swap (save->child [1], rhs);
                lhs = save->child [1];
            }
            else {
                std::swap (lhs, rhs);
            }

            lhs->p = save;
            rhs->p = 0;
        }

        if (lhs->child [1]) {
            lhs = lhs->child [1];
        }
        else {
            lhs->child [1] = rhs;
            rhs->p = lhs;
            break;
        }
    }

    return restore_property (rhs), ptr;
}

/**************************************************************************/

template< typename K, typename T, typename Off,
          typename R, typename C, typename A >
typename leftist_tree< K, T, Off, R, C, A >::iterator
leftist_tree< K, T, Off, R, C, A >::top ()
{
    return make_iter (m_end->child [0]);
}

template< typename K, typename T, typename Off,
          typename R, typename C, typename A >
typename leftist_tree< K, T, Off, R, C, A >::const_iterator
leftist_tree< K, T, Off, R, C, A >::top () const
{
    return make_iter (m_end->child [0]);
}

template< typename K, typename T, typename Off,
          typename R, typename C, typename A >
typename leftist_tree< K, T, Off, R, C, A >::iterator
leftist_tree< K, T, Off, R, C, A >::push (value_type const& val)
{
    node_pointer p = (node_pointer)node_allocator ().allocate (1U);
    ::memset (p, 0, sizeof * p);

    try {
        this->construct (&p->value, val);
    }
    catch (...) {
        node_allocator ().deallocate (p, 1);
        throw;
    }

    p->distance = 1;

    if (m_end->p == m_end) {
        m_end->child [0] = p;

        p->p = m_end;
        m_end->p = p;
    }
    else {
        // Perform the merge with root
        p = merge (m_end->p, p);

        p->p = m_end;
        m_end->p = p;

        m_end->child [0] = p;
    }

    return ++m_size, make_iter (p);
}

template< typename K, typename T, typename Off,
          typename R, typename C, typename A >
void
leftist_tree< K, T, Off, R, C, A >::pop ()
{
    if (m_end->p == m_end) {
        return;
    }

    node_pointer p, l, r;

    p = m_end->p;

    l = p->child [0];
    r = p->child [1];

    this->destroy (&p->value);
    node_allocator ().deallocate (p, 1);

    --m_size;

    p = merge (l, r);
    p = p ? p : m_end;

    p->p = m_end;
    m_end->p = m_end->child [0] = p;
}

template< typename K, typename T, typename Off,
          typename R, typename C, typename A >
void
leftist_tree< K, T, Off, R, C, A >::merge (tree_type& ref)
{
    node_pointer p, lhs, rhs;

    lhs = m_end->p;
    rhs = ref.m_end->p;

    p = merge (lhs, rhs);

    m_size += ref.m_size;
    ref.m_size = 0;

    p->p = m_end;
    m_end->p = p;

    m_end->child [0] = p;
    ref.m_end->child [0] = ref.m_end->p = ref.m_end;
}

template< typename K, typename T, typename Off,
          typename R, typename C, typename A >
void
leftist_tree< K, T, Off, R, C, A >::change_key (
    iterator iter, offset_type const& off)
{
    assert (iter != end ());

    node_pointer p = iter.m_node;
    assert (!m_comp (p->key (), p->key () + off));

    p->key (p->key () + off);

    if (p == m_end->p || !m_comp (p->key (), p->p->key ())) {
        return;
    }

    if (p == p->p->child [0]) {
        p->p->child [0] = 0;
    }
    else if (p == p->p->child [1]) {
        p->p->child [1] = 0;
    }

    m_end->p->p = 0;
    restore_property (p->p);

    p = merge (m_end->p, p);

    p->p = m_end;
    m_end->p = m_end->child [0] = p;
}

}}
