// -*- C++ -*-

#include <cc/defs.hpp>
#include <cc/detail/hb_tree.hpp>

#include <cstring>

#include <algorithm>

namespace cc {
namespace detail {

template< typename K, typename T, typename R, typename C, typename A >
inline void
hb_tree< K, T, R, C, A >::init ()
{
    m_end = (node_pointer)node_allocator ().allocate (1U);
    ::memset (m_end, 0, sizeof * m_end);

    m_end->p = m_end->child [0] = m_end->child [1] = m_end;
}

template< typename K, typename T, typename R, typename C, typename A >
/* explicit  */
hb_tree< K, T, R, C, A >::hb_tree (
    int n, key_compare const& comp, allocator_type const& alloc)
    : A (alloc), m_end (), m_size (), m_balance (n), m_comp (comp)
{
    init ();
}

template< typename K, typename T, typename R, typename C, typename A >
template< typename Iterator >
hb_tree< K, T, R, C, A >::hb_tree (
    Iterator first, Iterator last, bool dup, int n,
    key_compare const& comp, allocator_type const& alloc)
    : A (alloc), m_end (), m_size (), m_balance (n), m_comp (comp)
{
    init ();
    insert (first, last, dup);
}

template< typename K, typename T, typename R, typename C, typename A >
hb_tree< K, T, R, C, A >::hb_tree (hb_tree const& ref, bool dup)
    : A (ref), m_end (), m_size (), m_balance (ref.m_balance),
      m_comp (ref.key_comp ())
{
    init ();
    insert (ref.begin (), ref.end (), dup);
}

template< typename K, typename T, typename R, typename C, typename A >
hb_tree< K, T, R, C, A >&
hb_tree< K, T, R, C, A >::operator= (hb_tree const& ref)
{
    if (this != &ref) {
        hb_tree< K, T, R, C, A > tmp (ref);
        swap (tmp);
    }

    return *this;
}

template< typename K, typename T, typename R, typename C, typename A >
hb_tree< K, T, R, C, A >::~hb_tree ()
{
    clear ();
    node_allocator ().deallocate (m_end, 1U);
}

/**************************************************************************/

template< typename K, typename T, typename R, typename C, typename A >
inline typename hb_tree< K, T, R, C, A >::size_type
hb_tree< K, T, R, C, A >::max_distance (node_pointer p)
{
    assert (p);
    assert (p != m_end);

    int_type left = p->child [0] ? p->child [0]->distance : -1;
    int_type rite = p->child [1] ? p->child [1]->distance : -1;

    return (std::max) (left, rite) + 1;
}

template< typename K, typename T, typename R, typename C, typename A >
inline typename hb_tree< K, T, R, C, A >::int_type
hb_tree< K, T, R, C, A >::weigh (node_pointer p)
{
    assert (p);
    assert (p != m_end);

    int_type left = p->child [0] ? p->child [0]->distance : -1;
    int_type rite = p->child [1] ? p->child [1]->distance : -1;

    return rite - left;
}

template< typename K, typename T, typename R, typename C, typename A >
typename hb_tree< K, T, R, C, A >::node_pointer
hb_tree< K, T, R, C, A >::rotate_left (node_pointer p)
{

    assert (p && p != m_end);
    assert (p->child [1]);

    node_pointer parent = p;
    node_pointer child  = p->child [1];

    if (parent->p == m_end) {
        parent->p->p = child;
        child->p = parent->p;
    }
    else {
        if (parent == parent->p->child [0]) {
            parent->p->child [0] = child;
        }
        else if (parent == parent->p->child [1]) {
            parent->p->child [1] = child;
        }
        else {
            assert (false);
        }
    }

    if (child->child [0]) {
        child->child [0]->p = parent;
    }

    parent->child [1] = child->child [0];
    child->child [0] = parent;

    child->p = parent->p;
    parent->p = child;

    // adjust distances
    parent->distance = max_distance (parent);
    child->distance  = max_distance (child);

    return child;
}

template< typename K, typename T, typename R, typename C, typename A >
typename hb_tree< K, T, R, C, A >::node_pointer
hb_tree< K, T, R, C, A >::rotate_right (node_pointer p)
{
    assert (p && p != m_end);
    assert (p->child [0]);

    node_pointer parent = p;
    node_pointer child  = p->child [0];

    if (parent->p == m_end) {
        parent->p->p = child;
        child->p = parent->p;
    }
    else {
        if (parent == parent->p->child [0]) {
            parent->p->child [0] = child;
        }
        else if (parent == parent->p->child [1]) {
            parent->p->child [1] = child;
        }
        else {
            assert (false);
        }
    }

    if (child->child [1]) {
        child->child [1]->p = parent;
    }

    parent->child [0] = child->child [1];
    child->child [1] = parent;

    child->p = parent->p;
    parent->p = child;

    // adjust distances
    parent->distance = max_distance (parent);
    child->distance  = max_distance (child);

    return child;
}

/**********************************************************************/

template< typename K, typename T, typename R, typename C, typename A >
typename hb_tree< K, T, R, C, A >::iterator
hb_tree< K, T, R, C, A >::lower_bound (key_type const& k)
{
    node_pointer y = m_end;

    for (node_pointer x = y->p; x && x != m_end;) {
        if (m_comp (x->key (), k)) {
            x = x->child [1];
        }
        else {
            y = x;
            x = x->child [0];
        }
    }

    return make_iter (y);
}

template< typename K, typename T, typename R, typename C, typename A >
typename hb_tree< K, T, R, C, A >::iterator
hb_tree< K, T, R, C, A >::upper_bound (key_type const& k)
{
    node_pointer y = m_end;

    for (node_pointer x = y->p; x && x != m_end;) {
        if (m_comp (k, x->key ())) {
            y = x;
            x = x->child [0];
        }
        else {
            x = x->child [1];
        }
    }

    return make_iter (y);
}

template< typename K, typename T, typename R, typename C, typename A >
typename hb_tree< K, T, R, C, A >::iterator
hb_tree< K, T, R, C, A >::find (key_type const& k)
{
    if (empty ()) {
        return end ();
    }

    const iterator i = lower_bound (k);
    return i == end () || m_comp (k, i.m_node->key ()) ? end () : i;
}

template< typename K, typename T, typename R, typename C, typename A >
void
hb_tree< K, T, R, C, A >::restore_property (node_pointer p)
{
    // Climb up to the header
    for (; p && p != m_end; p = p->p) {

        p->distance = max_distance (p);

        for (;;) {
            // calculate the node balance
            int n = weigh (p);

            if (n >= m_balance) {
                // calculate the balance of the right child
                int right_balance = weigh (p->child [1]);

                // either a RR or a RL
                if (right_balance >= 0) {
                    // RR
                    p = rotate_left (p);
                }
                else {
                    // RL
                    rotate_right (p->child [1]);
                    p = rotate_left (p);
                }
            }
            else if (n <= 0 - m_balance) {
                // calculate the balance of the right child
                int left_balance = weigh (p->child [0]);

                // either a RR or a RL
                if (left_balance <= 0) {
                    // LL
                    p = rotate_right (p);
                }
                else {
                    // LR
                    rotate_left (p->child [0]);
                    p = rotate_right (p);
                }
            }
            else
                // this node is fixed
            {
                break;
            }
        }
    }
}

template< typename K, typename T, typename R, typename C, typename A >
void
hb_tree< K, T, R, C, A >::swap_nodes (node_pointer lhs, node_pointer rhs)
{
    assert (rhs);
    assert (lhs);

    assert (lhs->child [1] == 0);

    assert (lhs != m_end);
    assert (rhs != m_end);

    assert (lhs != rhs);

    // Update header links if pointing to them; lhs is always to the right of
    if (m_end->child [0] == lhs) {
        m_end->child [0] = rhs;
    }

    if (m_end->child [1] == rhs) {
        m_end->child [1] = lhs;
    }

    // Swap parents' links
    if (lhs->p == m_end) {
        int side = rhs->p->child [0] == rhs ? 0 : 1;
        std::swap (lhs->p->p, rhs->p->child [side]);
    }
    else if (rhs->p == m_end) {
        int side = lhs->p->child [0] == lhs ? 0 : 1;
        std::swap (rhs->p->p, lhs->p->child [side]);
    }
    else {
        int side [2] = { 0 };

        if (lhs->p->child [1] == lhs) {
            side [1] = 1;
        }

        if (rhs->p->child [1] == rhs) {
            side [0] = 1;
        }

        std::swap (lhs->p->child [side [1]], rhs->p->child [side [0]]);
    }

    // Swap parents
    std::swap (lhs->p, rhs->p);

    // Swap children links
    std::swap (lhs->child [0], rhs->child [0]);
    std::swap (lhs->child [1], rhs->child [1]);

    lhs->distance = max_distance (lhs);
    rhs->distance = max_distance (rhs);
}

/**************************************************************************/

template< typename K, typename T, typename R, typename C, typename A >
std::pair< typename hb_tree< K, T, R, C, A >::iterator, bool >
hb_tree< K, T, R, C, A >::insert (const_reference val, bool dup)
{
    node_pointer p = (node_pointer)node_allocator ().allocate (1);
    ::memset (p, 0, sizeof * p);

    try {
        this->construct (&p->value, val);
    }
    catch (...) {
        node_allocator ().deallocate (p, 1);
        throw;
    }

    // Set the distance
    p->distance = 0;

    // Shortcut: if no header, set this node as header
    if (m_end->p == m_end) {
        m_end->child [0] = m_end->child [1] = p;

        p->p = m_end;
        m_end->p = p;

        m_size = 1;

        return std::pair< iterator, bool > (make_iter (p), true);
    }

    // Otherwise, proceed to insert
    for (node_type* ptr = m_end->p;;) {

        int direction =
            m_comp (p->key (), ptr->key ()) ? -1 :
            m_comp (ptr->key (), p->key ()) ?  1 : 0;

        if (direction == 0 && !dup) {
            return std::pair< iterator, bool > (iterator (), false);
        }

        int side = direction > 0 ? 1 : 0;

        if (ptr->child [side]) {
            ptr = ptr->child [side];
        }
        else {
            ptr->child [side] = p;
            p->p = ptr;

            if (m_end->child [side] == ptr) {
                m_end->child [side] = p;
            }

            break;
        }
    }

    // restore balance in the tree
    restore_property (p);

    // Adjust size
    ++m_size;

    return std::pair< iterator, bool > (make_iter (p), true);
}

// Erase element identified by iterator
template< typename K, typename T, typename R, typename C, typename A >
void hb_tree< K, T, R, C, A >::erase (iterator pos)
{
    // Should never be the end
    if (pos == end ()) {
        return;
    }

    // The node
    node_pointer p = pos.m_node;

    // The replacement node -- where the re-balancing starts
    node_pointer restore_node = 0;

    int mask = (p->child [0] ? 2 : 0) | (p->child [1] ? 1 : 0);

    switch (mask) {
    case 0:
        if (p->p == m_end) {
            m_end->p = m_end->child [0] = m_end->child [1] = m_end;
        }
        else {
            if (p->p->child [0] == p) {
                p->p->child [0] = 0;

                // This node could be a leftmost node
                if (m_end->child [0] == p) {
                    m_end->child [0] = p->p;
                }
            }
            else if (p->p->child [1] == p) {
                p->p->child [1] = 0;

                // May be a rightmost node
                if (m_end->child [1] == p) {
                    m_end->child [1] = p->p;
                }
            }
            else {
                assert (false);
            }
        }

        restore_node = p->p;
        break;

    case 1:

        // This node gets replaced with its right child.
        if (p->p == m_end) {
            m_end->p = p->child [1];
            p->child [1]->p = m_end;

            // This node is a leftmost node
            m_end->child [0] = p->child [1];
        }
        else {
            if (p->p->child [0] == p) {
                p->child [1]->p = p->p;
                p->p->child [0] = p->child [1];

                // May be a leftmost node
                if (m_end->child [0] == p) {
                    m_end->child [0] = p->child [1];
                }
            }
            else if (p->p->child [1] == p) {
                p->child [1]->p = p->p;
                p->p->child [1] = p->child [1];
            }
            else {
                assert (false);
            }
        }

        restore_node = p->child [1];
        break;

    case 2:

        // This node gets replaced with its left child.
        if (p->p == m_end) {
            m_end->p = p->child [0];
            p->child [0]->p = m_end;

            // This node is a leftmost node
            m_end->child [1] = p->child [0];
        }
        else {
            if (p->p->child [0] == p) {
                p->child [0]->p = p->p;
                p->p->child [0] = p->child [0];
            }
            else if (p->p->child [1] == p) {
                p->child [0]->p = p->p;
                p->p->child [1] = p->child [0];

                // May be a rightmost child
                if (m_end->child [1] == p) {
                    m_end->child [1] = p->child [0];
                }
            }
            else {
                assert (false);
            }
        }

        restore_node = p->child [0];
        break;

    case 3:
        // This node gets replaced with rightmost child
        // of left subtree and then proceed to delete
        // that node.
        node_pointer ptr = node_type::rightmost (p->child [0]);
        swap_nodes (p, ptr);

        // After swap, node points to the bottom of the tree;
        // recursively call erase to erase this node.
        erase (make_iter (p));

        // Rebalancing occurred during the recursive call
        return;
    }

    // Deallocate
    this->destroy (&p->value);
    node_allocator ().deallocate (p, 1U);

    // adjust size
    --m_size;

    // Restore the height balance
    restore_property (restore_node);
}

}
}
