// -*- C++ -*-

#ifndef CC_HB_MAP_HPP
#define CC_HB_MAP_HPP

#include <cc/defs.hpp>
#include <cc/detail/hb_tree.hpp>

#include <memory>
#include <functional>

namespace cc {

template< typename K, typename T, typename Comp = std::less< K >,
          typename Alloc = std::allocator< std::pair< K const, T > > >
struct hb_map {
    typedef K  key_type;
    typedef T  mapped_type;

    typedef std::pair< key_type const, mapped_type > value_type;

    typedef Comp  key_compare;
    typedef Alloc allocator_type;

private:

    typedef
    ::cc::detail::hb_tree< key_type, value_type,
                           cc::detail::key_ref< value_type, key_type >,
                           key_compare, allocator_type > tree_type;

    tree_type m_tree;

public:

    typedef typename tree_type::pointer                pointer;
    typedef typename tree_type::const_pointer          const_pointer;

    typedef typename tree_type::reference              reference;
    typedef typename tree_type::const_reference        const_reference;

    typedef typename tree_type::size_type              size_type;
    typedef typename tree_type::difference_type        difference_type;


    typedef typename tree_type::iterator               iterator;
    typedef typename tree_type::const_iterator         const_iterator;

    typedef typename tree_type::reverse_iterator       reverse_iterator;
    typedef typename tree_type::const_reverse_iterator const_reverse_iterator;


    struct value_compare : std::binary_function< value_type, value_type, bool > {
        friend struct hb_map;

        bool operator ()(value_type const& lhs, value_type const& rhs) const {
            return comp (lhs.first, rhs.first);
        }

    protected:
        explicit value_compare (key_compare cmp) : comp (cmp) { }

    protected:
        key_compare comp;
    };

    explicit
    hb_map (int bal = 2, key_compare const& cmp  = key_compare (),
            allocator_type const& alloc = allocator_type ())
        : m_tree (bal, cmp, alloc) {
    }

    template< typename InputIterator >
    hb_map (InputIterator first, InputIterator last, int bal = 2,
            key_compare const& cmp  = key_compare (),
            allocator_type const& alloc = allocator_type ())
        : m_tree (first, last, false, bal, cmp, alloc) {
    }

    hb_map (hb_map const& rhs): m_tree (rhs.m_tree) { }

    hb_map& operator= (hb_map const& rhs) {
        return m_tree = rhs.m_tree, *this;
    }

    allocator_type get_allocator () const {
        return m_tree.get_allocator ();
    }

    int balance () const {
        return m_tree.balance ();
    }

    iterator begin () {
        return m_tree.begin ();
    }

    const_iterator begin () const {
        return m_tree.begin ();
    }

    iterator end () {
        return m_tree.end ();
    }

    const_iterator end () const {
        return m_tree.end ();
    }

    reverse_iterator rbegin () {
        return m_tree.rbegin ();
    }

    const_reverse_iterator rbegin () const {
        return m_tree.rbegin ();
    }

    reverse_iterator rend () {
        return m_tree.rend ();
    }

    const_reverse_iterator rend () const {
        return m_tree.rend ();
    }

    bool empty () const {
        return m_tree.empty ();
    }

    size_type size () const {
        return m_tree.size ();
    }

    size_type max_size () const {
        return m_tree.max_size ();
    }

    mapped_type& operator[] (key_type const& key) {
        return insert (value_type (key, mapped_type ())).first->second;
    }

    std::pair< iterator, bool > insert (value_type const& ref) {
        return m_tree.insert (ref, false);
    }

    iterator insert (iterator iter, value_type const& ref) {
        return m_tree.insert (iter, ref, false);
    }

    template< typename InputIterator >
    void insert (InputIterator first, InputIterator last) {
        m_tree.insert (first, last, false);
    }

    void erase (iterator iter) {
        m_tree.erase (iter);
    }

    size_type erase (const key_type& key) {
        return m_tree.erase (key);
    }

    void  erase (iterator first, iterator last) {
        m_tree.erase (first, last);
    }

    void swap (hb_map& ref) {
        m_tree.swap (ref.m_tree);
    }

    void clear () {
        erase (begin (), end ());
    }

    key_compare key_comp () const {
        return m_tree.key_comp ();
    }

    value_compare value_comp () const {
        return value_compare (m_tree.key_comp ());
    }

    iterator find (key_type const& key) {
        return m_tree.find (key);
    }

    const_iterator find (key_type const& key) const {
        return m_tree.find (key);
    }

    size_type count (key_type const& key) const {
        return m_tree.count (key);
    }

    iterator lower_bound (key_type const& key) {
        return m_tree.lower_bound (key);
    }

    iterator upper_bound (key_type const& key) {
        return m_tree.upper_bound (key);
    }

    const_iterator lower_bound (key_type const& key) const {
        return m_tree.lower_bound (key);
    }

    const_iterator upper_bound (key_type const& key) const {
        return m_tree.upper_bound (key);
    }

    std::pair< iterator, iterator > equal_range (key_type const& key) {
        return m_tree.equal_range (key);
    }

    std::pair< const_iterator, const_iterator >
    equal_range (key_type const& key) const {
        return m_tree.equal_range (key);
    }
};


template< typename K, typename T,
          typename Comp = std::less< K >,
          typename Alloc = std::allocator< std::pair< K const, T > > >
struct hb_multimap {
public:

    typedef K  key_type;
    typedef T  mapped_type;

    typedef std::pair< key_type const, mapped_type >  value_type;

    typedef Comp   key_compare;
    typedef Alloc  allocator_type;

private:

    typedef
    ::cc::detail::hb_tree< key_type, value_type,
                           cc::detail::key_ref< value_type, key_type >,
                           key_compare, allocator_type > tree_type;

    tree_type m_tree;

public:

    typedef typename tree_type::pointer                pointer;
    typedef typename tree_type::const_pointer          const_pointer;

    typedef typename tree_type::reference              reference;
    typedef typename tree_type::const_reference        const_reference;

    typedef typename tree_type::size_type              size_type;
    typedef typename tree_type::difference_type        difference_type;

    typedef typename tree_type::iterator               iterator;
    typedef typename tree_type::const_iterator         const_iterator;

    typedef typename tree_type::reverse_iterator       reverse_iterator;
    typedef typename tree_type::const_reverse_iterator const_reverse_iterator;

public:

    struct value_compare : std::binary_function< value_type, value_type, bool > {
        friend struct hb_multimap;

        bool operator () (value_type const& lhs, value_type const& rhs) const {
            return comp (lhs.first, rhs.first);
        }

    protected:
        explicit value_compare (key_compare cmp): comp (cmp) { }

    protected:
        key_compare comp;
    };

public:

    explicit
    hb_multimap (int bal = 2,
                 key_compare const& cmp = key_compare (),
                 allocator_type const& alloc = allocator_type ())
        : m_tree (bal, cmp, alloc) {
    }

    template< typename InputIterator >
    hb_multimap (InputIterator first, InputIterator last, int bal = 2,
                 key_compare const& cmp  = key_compare (),
                 allocator_type const& alloc = allocator_type ())
        : m_tree (first, last, true, bal, cmp, alloc) {
    }

    hb_multimap (hb_multimap const& rhs)
        : m_tree (rhs.m_tree, true) {
    }

    hb_multimap& operator= (hb_multimap const& rhs) {
        return m_tree = rhs.m_tree, *this;
    }

    int balance () const {
        return m_tree.balance ();
    }

    allocator_type get_allocator () const {
        return m_tree.get_allocator ();
    }

    iterator begin () {
        return m_tree.begin ();
    }

    const_iterator begin () const {
        return m_tree.begin ();
    }

    iterator end () {
        return m_tree.end ();
    }

    const_iterator end () const {
        return m_tree.end ();
    }

    reverse_iterator rbegin () {
        return m_tree.rbegin ();
    }

    const_reverse_iterator rbegin () const {
        return m_tree.rbegin ();
    }

    reverse_iterator rend () {
        return m_tree.rend ();
    }

    const_reverse_iterator rend () const {
        return m_tree.rend ();
    }

    bool empty () const {
        return m_tree.empty ();
    }

    size_type size () const {
        return m_tree.size ();
    }

    size_type max_size () const {
        return m_tree.max_size ();
    }

    iterator insert (const value_type& ref) {
        return m_tree.insert (ref, true).first;
    }

    iterator insert (iterator iter, const value_type& ref) {
        return m_tree.insert (iter, ref, true);
    }

    template< typename InputIterator >
    void insert (InputIterator first, InputIterator last) {
        m_tree.insert (first, last, true);
    }

    void erase (iterator iter) {
        m_tree.erase (iter);
    }

    size_type erase (key_type const& key) {
        return m_tree.erase (key);
    }

    void erase (iterator first, iterator last) {
        m_tree.erase (first, last);
    }

    void clear () {
        erase (begin (), end ());
    }

    void swap (hb_multimap& ref) {
        m_tree.swap (ref.m_tree);
    }

    key_compare  key_comp () const {
        return m_tree.key_comp ();
    }

    value_compare value_comp () const {
        return value_compare (m_tree.key_comp ());
    }

    iterator find (key_type const& key) {
        return m_tree.find (key);
    }

    const_iterator find (key_type const& key) const {
        return m_tree.find (key);
    }

    size_type count (key_type const& key) const {
        return m_tree.count (key);
    }

    iterator lower_bound (key_type const& key) {
        return m_tree.lower_bound (key);
    }

    iterator upper_bound (key_type const& key) {
        return m_tree.upper_bound (key);
    }

    const_iterator lower_bound (key_type const& key) const {
        return m_tree.lower_bound (key);
    }

    const_iterator upper_bound (key_type const& key) const {
        return m_tree.upper_bound (key);
    }

    std::pair< iterator, iterator >
    equal_range (key_type const& key) {
        return m_tree.equal_range (key);
    }

    std::pair< const_iterator, const_iterator >
    equal_range (key_type const& key) const {
        return m_tree.equal_range (key);
    }
};

} // namespace cc

#endif // CC_HB_MAP_HPP
