// -*- C++ -*-

#ifndef CC_LEFTIST_SET_HPP
#define CC_LEFTIST_SET_HPP

#include <cc/detail/leftist_tree.hpp>

#include <memory>
#include <functional>

namespace cc {

template< typename K, typename Off = K,
          typename Comp = std::less< K >,
          typename Alloc = std::allocator< K > >
struct leftist_set {
    typedef K     key_type;
    typedef K     value_type;

    typedef Off   offset_type;

    typedef Comp  key_compare;
    typedef Comp  value_compare;

    typedef Alloc allocator_type;

private:

    typedef
    ::cc::detail::leftist_tree< key_type, key_type, offset_type,
                                cc::detail::key_ref< value_type, key_type >,
                                key_compare, allocator_type > tree_type;

    tree_type m_tree;

public:

    typedef typename tree_type::pointer                pointer;
    typedef typename tree_type::const_pointer          const_pointer;

    typedef typename tree_type::reference              reference;
    typedef typename tree_type::const_reference        const_reference;

    typedef typename tree_type::size_type              size_type;
    typedef typename tree_type::difference_type        difference_type;


    typedef typename tree_type::iterator               iterator;
    typedef typename tree_type::const_iterator         const_iterator;

    typedef typename tree_type::reverse_iterator       reverse_iterator;
    typedef typename tree_type::const_reverse_iterator const_reverse_iterator;

    explicit
    leftist_set (key_compare const& cmp = key_compare (),
                 allocator_type const& alloc = allocator_type ())
        : m_tree (cmp, alloc) {
    }

    template< typename InputIterator >
    leftist_set (InputIterator first, InputIterator last,
                 Comp const& cmp = Comp (), Alloc const& alloc = Alloc ())
        : m_tree (first, last, cmp, alloc) {
    }

    leftist_set (leftist_set const& rhs): m_tree (rhs.m_tree) { }

    leftist_set& operator= (leftist_set const& rhs) {
        return m_tree = rhs.m_tree, *this;
    }

    allocator_type get_allocator () const {
        return m_tree.get_allocator ();
    }

    iterator begin () {
        return m_tree.begin ();
    }

    const_iterator begin () const {
        return m_tree.begin ();
    }

    iterator end () {
        return m_tree.end ();
    }

    const_iterator end () const {
        return m_tree.end ();
    }

    reverse_iterator rbegin () {
        return m_tree.rbegin ();
    }

    const_reverse_iterator rbegin () const {
        return m_tree.rbegin ();
    }

    reverse_iterator rend () {
        return m_tree.rend ();
    }

    const_reverse_iterator rend () const {
        return m_tree.rend ();
    }

    bool empty () const {
        return m_tree.empty ();
    }

    size_type size () const {
        return m_tree.size ();
    }

    size_type max_size () const {
        return m_tree.max_size ();
    }

    iterator top () {
        return m_tree.top ();
    }

    const_iterator top () const {
        return m_tree.top ();
    }

    iterator push (value_type const& value) {
        return m_tree.push (value);
    }

    void pop () {
        m_tree.pop ();
    }

    void merge (leftist_set& other) {
        m_tree.merge (other.m_tree);
    }

    void change_key (iterator iter, offset_type const& off) {
        m_tree.change_key (iter, off);
    }

    void swap (leftist_set& other) {
        m_tree.swap (other.m_tree);
    }

    void clear () {
        m_tree.clear ();
    }

    key_compare key_comp () const {
        return m_tree.key_comp ();
    }

    value_compare value_comp () const {
        return value_compare (m_tree.key_comp ());
    }
};

} // namespace cc

#endif // CC_TREE_LEFTIST_SET_HPP
