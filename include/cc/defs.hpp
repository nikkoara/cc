// -*- C++ -*-

#ifndef CC_DEFS_HPP
#define CC_DEFS_HPP

#include <cc/config.hpp>

#define CC_REBIND(from,to) typename from::template rebind < to >::other

#if !defined (CC_DEFAULT_HBTREE_BALANCE)
#  define CC_DEFAULT_HBTREE_BALANCE        2
#endif // CC_DEFAULT_HBTREE_BALANCE

#if !defined (CC_PHI_RATIO)
#  define CC_PHI_RATIO                1.618f
#endif // CC_PHI_RATIO

#if !defined (CC_DOUBLE_RATIO)
#  define CC_DOUBLE_RATIO             2.0f
#endif // CC_DOUBLE_RATIO

#include <cstddef>
#include <cassert>

#define CC_ASSERT(x) assert(x)

#define NAMESPACE(x)   namespace x {
#define NAMESPACE_END()  }

NAMESPACE(cc)
NAMESPACE_END()

#define CC_UNUSED(x) ((void)x)

#define CC_DO_CAT(a,b) a ## b
#define CC_CAT(a,b) CC_DO_CAT (a, b)

#endif // CC_DEFS_HPP
